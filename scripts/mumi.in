#!@GUILE@ --no-auto-compile
-*- scheme -*-
-*- geiser-scheme-implementation: guile -*-
!#
;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2016, 2017, 2019, 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2018, 2021, 2023–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This file is part of mumi.
;;;
;;; mumi is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; mumi is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with mumi.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (srfi srfi-1)
             (srfi srfi-26)
             (srfi srfi-37)
             (srfi srfi-71)
             (system repl server)
             (ice-9 match)
             (ice-9 format)
             ((mumi client) #:prefix client:)
             (mumi config)
             ((mumi debbugs)
              #:select (extract-bug-numbers))
             ((mumi jobs)
              #:select (worker-loop))
             ((mumi web server)
              #:select (start-mumi-web-server))
             ((mumi xapian)
              #:select (index!)))

(define %default-repl-server-port
  ;; Default port to run REPL server on, if --listen-repl is provided
  ;; but no port is mentioned
  37146)

(setvbuf (current-output-port) 'line)
(setvbuf (current-error-port) 'line)

;; Ensure COLUMNS is set, as this being unset can cause displaying backtraces to
;; crash in some versions of Guile
(let ((columns (string->number (or (getenv "COLUMNS") ""))))
  (setenv "COLUMNS"
          (number->string
           (if columns
               (max 256 columns)
               256))))

;; Don't make lots of unnecessary readlink syscalls when opening files
(fluid-set! %file-port-name-canonicalization 'none)

(with-exception-handler
    (lambda (exn)
      (simple-format #t "failed increasing open file limit: ~A\n" exn))
  (lambda ()
    (setrlimit 'nofile 4096 4096))
  #:unwind? #t)

;; Keep indexing the mail directory
(define %update-interval 30)

(define* (update-state! #:key
                        (dbpath (string-append (%config 'db-dir) "/mumi.xapian"))
                        loop?
                        #:rest rest)
  (catch #t
    (lambda ()
      (index! #:dbpath dbpath)
      (when loop?
        (format (current-error-port)
                "Sleeping for ~a seconds.~%" %update-interval)
        (sleep %update-interval)
        (apply update-state! rest)))
    (lambda args
      (format (current-error-port) "worker error: ~a~%" args)
      (sleep %update-interval)
      (apply update-state! rest))))

(define %config-file-option
  (option '("config") #t #f
          (lambda (opt name arg result)
            (alist-cons 'config-file arg
                        (alist-delete 'config-file result)))))

(define %options
  ;; Specifications of the command-line options
  (list (option '("address") #t #f
                (lambda (opt name arg result)
                  (catch #t
                    (lambda ()
                      (inet-pton AF_INET arg))
                    (lambda _
                      (error "invalid web server address" arg)))
                  (alist-cons 'address arg
                              (alist-delete 'address result))))
        (option '("port") #t #f
                (lambda (opt name arg result)
                  (let ((port (string->number arg)))
                    (if port
                        (alist-cons 'port port
                                    (alist-delete 'port result))
                        (error "invalid web server port" arg)))))
        (option '("listen-repl") #f #t
                (lambda (opt name arg result)
                  (alist-cons 'listen-repl (or (string->number arg) arg)
                              (alist-delete 'listen-repl result))))
        %config-file-option
        (option '("sender") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'sender arg result)))
        (option '("smtp") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'smtp arg result)))))

(define %default-options
  ;; Alist of default option values
  `((listen-repl . #f)
    (smtp . #f)
    (sender . #f)
    (address . "0.0.0.0")
    (port . 1234)))

(define (parse-options args)
  (args-fold
   args %options
   (lambda (opt name arg result)
     (error "unrecognized option" name))
   (lambda (arg result)
     (error "extraneous argument" arg))
   %default-options))

(define (show-mumi-usage)
  (format (current-error-port)
          "
    `mumi search QUERY':
         search mumi for issues.

    `mumi current [ISSUE-NUMBER]':
         print or set current issue.

    `mumi new':
         clear current issue presumably to open a new one.

    `mumi www':
         open current issue in a web browser.

    `mumi compose [--close|--done]':
         compose email to debbugs.

    `mumi command [--reassign=package] [-s|--severity=severity] [-m|--merge=issue] [-t|--tag=tag]':
         compose email to control server.

    `mumi am [--dry-run|-n] [spec] -- [git-am-options]':
         apply patches.

    `mumi send-email':
         send patches to debbugs.

    `mumi web [--address=address] [--port=port] [--listen-repl[=port]] [--config=file]':
         start the application web server.

    `mumi mailer --sender=SENDER --smtp=SMTP [--config=file]:
         start a mailer process (requires Redis).

    `mumi worker [--config=file]':
         run an update loop to refresh issue information from Debbugs.

    `mumi fetch [--config=file] [xapian-dbpath]':
         index all Debbugs bug logs and update bug statuses once.

~%")
  (exit 1))

(define (list-partition lst delimiter)
  "Split LST into two: one with elements before and the other with
elements after the DELIMITER."
  (let ((first-half second-half (break (cut equal? delimiter <>)
                                       lst)))
    (values first-half
            (match second-half
              ((delimiter tail ...) tail)
              (() (list))))))

(define (server-config-from-file config-file)
  ;; Read config file and override defaults.
  (append (if config-file
              (call-with-input-file config-file read)
              '())
          (%server-config)))

(match (cdr (program-arguments))
  (("search" . query-strings)
   (client:search (string-join query-strings)))
  (("current")
   (client:print-current-issue))
  (("current" issue-number-string)
   (let ((issue-number (string->number issue-number-string)))
     (if issue-number
         (client:set-current-issue! issue-number)
         (begin
           (format (current-error-port)
                   "Invalid issue number `~a'~%"
                   issue-number-string)
           (exit #f))))
   (client:print-current-issue))
  (("new")
   (client:clear-current-issue!))
  (("send-email" . patches)
   (client:send-email (car (program-arguments)) patches))
  (("git-send-email-headers" patch)
   (client:git-send-email-headers patch))
  (("www")
   (client:www))
  (("compose" . args)
   (let ((args (args-fold args
                          (list (option '("close") #f #f
                                        (lambda (opt name arg result)
                                          (alist-cons 'close #t result)))
                                (option '("done") #f #f
                                        (lambda (opt name arg result)
                                          (alist-cons 'close #t result)))
                                (option '(#\r "reply-to") #t #f
                                        (lambda (opt name arg result)
                                          (alist-cons 'reply-to arg result))))
                          (lambda (opt name arg result)
                            (error "Unrecognized option" name))
                          (lambda (arg result)
                            (error "Extraneous argument" arg))
                          (list))))
     (client:compose-email #:close? (assq-ref args 'close)
                           #:reply-to-spec (assq-ref args 'reply-to))))
  (("command" . args)
   (let ((args (args-fold args
                          (list (option '("reassign") #t #f
                                        (lambda (opt name arg result)
                                          (alist-cons 'reassign arg
                                                      result)))
                                (option '(#\s "severity") #t #f
                                        (lambda (opt name arg result)
                                          (alist-cons 'severity arg
                                                      result)))
                                (option '(#\m "merge") #t #f
                                        (lambda (opt name arg result)
                                          (alist-cons 'merge
                                                      (cons (string->number arg)
                                                            (assq-ref result 'merge))
                                                      result)))
                                (option '(#\t "tag") #t #f
                                        (lambda (opt name arg result)
                                          (alist-cons 'tags
                                                      (cons (if (or (string-prefix? "+" arg)
                                                                    (string-prefix? "-" arg))
                                                                arg
                                                                (string-append "+" arg))
                                                            (assq-ref result 'tags))
                                                      result))))
                          (lambda (opt name arg result)
                            (error "Unrecognized option" name))
                          (lambda (arg result)
                            (error "Extraneous argument" arg))
                          '((merge . ())
                            (tags . ())))))
     (client:command-email #:reassign (assq-ref args 'reassign)
                           #:severity (assq-ref args 'severity)
                           #:merge (reverse (assq-ref args 'merge))
                           #:tags (reverse (assq-ref args 'tags)))))
  (("am" . args)
   (let* ((mumi-am-args git-am-args (list-partition args "--"))
          (args (args-fold mumi-am-args
                           (list (option '(#\n "dry-run") #f #f
                                         (lambda (opt name arg result)
                                           (alist-cons 'dry-run? #t result))))
                           (lambda (opt name arg result)
                             (error "Unrecognized option" name))
                           (lambda (arg result)
                             (alist-cons 'spec arg result))
                           (list))))
     (client:am #:spec (assq-ref args 'spec)
                #:dry-run? (assq-ref args 'dry-run?)
                #:git-am-args git-am-args)))
  (("mailer" . rest)
   (let* ((opts (parse-options rest))
          (sender (assoc-ref opts 'sender))
          (smtp (assoc-ref opts 'smtp)))
     (parameterize ((%server-config (server-config-from-file
                                     (assq-ref opts 'config-file))))
       (if (and sender smtp)
           (worker-loop opts)
           (error "Both sender and smtp options must be provided!")))))
  (("fetch" . rest)
   (let ((opts (args-fold rest
                          (list %config-file-option)
                          (lambda (opt name arg result)
                            (error "unrecognized option" name))
                          (lambda (arg result)
                            (alist-cons 'xapian-dbpath arg
                                        result))
                          '())))
     (parameterize ((%server-config (server-config-from-file
                                     (assq-ref opts 'config-file))))
       (apply update-state!
              #:loop? #f
              (cond
               ((assq-ref opts 'xapian-dbpath) => (cut list #:dbpath <>))
               (else '()))))))
  (("worker" . rest)
   (let ((opts (args-fold rest
                          (list %config-file-option)
                          (lambda (opt name arg result)
                            (error "unrecognized option" name))
                          (lambda (arg result)
                            (error "extraneous argument" arg))
                          '())))
     (parameterize ((%server-config (server-config-from-file
                                     (assq-ref opts 'config-file))))
       (update-state! #:loop? #t))))
  (("web" . rest)
   (let ((opts (parse-options rest)))
     (parameterize ((%server-config (server-config-from-file
                                     (assq-ref opts 'config-file))))
       (let ((listen-repl (assoc-ref opts 'listen-repl)))
         (when listen-repl
           (cond
            ((number? listen-repl)
             (format (current-error-port)
                     "REPL server listening on port ~a~%"
                     listen-repl)
             (spawn-server (make-tcp-server-socket #:port listen-repl)))
            (else
             (format (current-error-port)
                     "REPL server listening on ~a~%"
                     listen-repl)
             (spawn-server (make-unix-domain-server-socket #:path listen-repl))))))
       (start-mumi-web-server (assoc-ref opts 'address)
                              (assoc-ref opts 'port)))))
  (_ (show-mumi-usage)))
