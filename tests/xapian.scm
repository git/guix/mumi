;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2023–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (test-xapian)
  #:use-module (mumi config)
  #:use-module (mumi issue)
  #:use-module (mumi xapian)
  #:use-module (mumi test-utils)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 match))

(test-begin "xapian")

(define (time->datestamp time)
  (date->string (time-utc->date time) "~Y~m~d"))

(define resolve-date-limit
  (@@ (mumi xapian) resolve-date-limit))

(test-equal "resolve-date-limit: turns \"today\" into datestamp"
  (resolve-date-limit "today")
  (time->datestamp (current-time)))

(test-equal "resolve-date-limit: turns \"now\" into datestamp"
  (resolve-date-limit "now")
  (time->datestamp (current-time)))

(test-equal "resolve-date-limit: turns \"yesterday\" into datestamp"
  (resolve-date-limit "yesterday")
  (let* ((today (current-time))
         (yesterday (subtract-duration today (make-time time-duration 0 (* 60 60 24)))))
    (time->datestamp yesterday)))

(test-equal "resolve-date-limit: pads short dates"
  (resolve-date-limit "20")
  "20000000")

(test-equal "resolve-date-limit: turns 1m into datestamp one month ago"
  (resolve-date-limit "1m")
  (let* ((today (current-time))
         (1m (subtract-duration today (make-time time-duration 0 (* 60 60 24 30)))))
    (time->datestamp 1m)))

(define data-dir
  (string-append (getenv "abs_top_srcdir") "/tests/data"))
(define db-dir (tmpnam))
(mkdir db-dir)

(parameterize ((%server-config
                '((cache-ttl . 10)
                  (packages
                   ("guix" (debbugs-packages . ("guix" "guix-patches")))))))
  (mock ((mumi config) %config
         (match-lambda
           ('db-dir db-dir)
           ('data-dir data-dir)))
        (index! #:dbpath (string-append db-dir "/mumi.xapian"))
        (dynamic-wind
          (lambda () #t)
          (lambda ()
            (test-assert "search: finds simple strings"
              (let ((result (map issue-number (search "hello" #:pagesize 2))))
                (and (= 2 (length result))
                     (member 33299 result)
                     (member 47187 result))))
            (test-equal "search: supports submitter prefix with partial name"
              '(26095)
              (map issue-number
                   (search "submitter:Ricardo" #:pagesize 2)))
            (test-equal "search: supports submitter prefix with partial email address"
              '(26095)
              (map issue-number
                   (search "submitter:rekado" #:pagesize 2)))
            (test-equal "search: supports submitter prefix with phrase"
              '(26095)
              (map issue-number
                   (search "submitter:\"Ricardo Wurmus\"" #:pagesize 2)))
            (test-assert "search: supports author prefix with email address"
              (let ((result (map issue-number (search "author:ludo" #:pagesize 2))))
                (and (= 2 (length result))
                     (member 26095 result)
                     (member 33299 result))))
            (test-equal "search: finds by message id"
              '(33299)
              (map issue-number
                   (search "msgid:c78be403-0616-67a0-fd5a-e1196b6a14d1@example.com"
                           #:pagesize 2)))
            (test-equal "search: finds by long message id"
              '(47187)
              (map issue-number
                   (search "msgid:X0AJfvGmJvZOXkqcxiL1wDpQGbPYwaMG5V24ltJiXsvMhc8i8OZkWd_uAf18tMpgcSq1izVJTiurVFRaflG2_dOtTi7UzrOZwT9DcV0gFo0=@protonmail.com"
                           #:pagesize 2))))
          (lambda ()
            (delete-file-recursively db-dir)))))

(test-end "xapian")
