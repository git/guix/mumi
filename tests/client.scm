;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2023–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (srfi srfi-26)
             (srfi srfi-64)
             (ice-9 match))

(define (with-variable variable value thunk)
  "Set VARIABLE to VALUE, run THUNK and restore the old value of
VARIABLE. Return the value returned by THUNK."
  (let ((old-value (variable-ref variable)))
    (dynamic-wind
      (cut variable-set! variable value)
      thunk
      (cut variable-set! variable old-value))))

(define (with-variables variable-bindings thunk)
  "Set VARIABLE-BINDINGS, run THUNK and restore the old values of the
variables. Return the value returned by THUNK. VARIABLE-BINDINGS is a
list of pairs mapping variables to their values."
  (match variable-bindings
    (((variable . value) tail ...)
     (with-variable variable value
       (cut with-variables tail thunk)))
    (() (thunk))))

(define-syntax-rule (var@@ module-name variable-name)
  (module-variable (resolve-module 'module-name)
                   'variable-name))

(define (trace-calls function-variable thunk)
  "Run THUNK and return a list of argument lists FUNCTION-VARIABLE is
called with."
  (let ((args-list (list)))
    (with-variable function-variable (lambda args
                                       (set! args-list
                                             (cons args args-list)))
      thunk)
    (reverse args-list)))

(define client-config-stub
  (cons (var@@ (mumi client) client-config)
        (lambda (key)
          (case key
            ((debbugs-host) "example.com")
            ((patch-email-address) "foo@patches.com")
            (else (error "Key unimplemented in stub" key))))))

(define do-not-poll-server-for-issue-number
  (cons (var@@ (mumi client) issue-number-of-message)
        (lambda _
          (error "Do not poll server for issue number"))))

(define set-current-issue-stub
  (cons (var@@ (mumi client) set-current-issue!)
        (const #t)))

(define serialize-email-address
  (@@ (mumi client) serialize-email-address))

(define split-cc
  (@@ (mumi client) split-cc))

(define parse-patch-subject
  (@@ (mumi client) parse-patch-subject))

(define patch-reroll-count
  (@@ (mumi client) patch-reroll-count))

(define patch-patch-index
  (@@ (mumi client) patch-patch-index))

(define patch-total-patches
  (@@ (mumi client) patch-total-patches))

(test-begin "client")

(test-equal "serialize email address"
  "Foo <foo@example.com>"
  (serialize-email-address "Foo" "foo@example.com"))

(test-equal "serialize email address with comma in name"
  "\"Bar, Foo\" <foobar@example.com>"
  (serialize-email-address "Bar, Foo" "foobar@example.com"))

(test-equal "split Cc field"
  (list "Foo <foo@example.com>"
        "\"Bar, Foo\" <foobar@example.com>")
  (split-cc "Foo <foo@example.com>, \"Bar, Foo\" <foobar@example.com>"))

(test-equal "send patches to new issue"
  '(("git" "send-email" "--to=foo@patches.com" "foo.patch")
    ("git" "send-email" "--to=12345@example.com" "bar.patch" "foobar.patch"))
  (map (match-lambda
         ((command _) command))
       (trace-calls (var@@ (mumi client) call-with-input-pipe*)
         (lambda ()
           (with-variables (list (cons (var@@ (mumi client) current-issue-number)
                                       (const #f))
                                 (cons (var@@ (mumi client) issue-number-of-message)
                                       (const 12345))
                                 client-config-stub
                                 set-current-issue-stub)
             (cut (@@ (mumi client) send-email)
                  "mumi"
                  (list "foo.patch" "bar.patch" "foobar.patch")))))))

(test-equal "send patches to existing issue"
  '(("git" "send-email" "--to=12345@example.com"
     "--header-cmd=mumi git-send-email-headers"
     "foo.patch" "bar.patch" "foobar.patch"))
  (map (match-lambda
         ((command _) command))
       (trace-calls (var@@ (mumi client) call-with-input-pipe*)
         (lambda ()
           (with-variables (list (cons (var@@ (mumi client) current-issue-number)
                                       (const 12345))
                                 (cons (var@@ (mumi client) reply-email-headers)
                                       (const '()))
                                 client-config-stub
                                 do-not-poll-server-for-issue-number)
             (cut (@@ (mumi client) send-email)
                  "mumi"
                  (list "foo.patch" "bar.patch" "foobar.patch")))))))

(test-equal "send single patch to new issue"
  '(("git" "send-email" "--to=foo@patches.com" "foo.patch"))
  (map (match-lambda
         ((command _) command))
       (trace-calls (var@@ (mumi client) call-with-input-pipe*)
         (lambda ()
           (with-variables (list (cons (var@@ (mumi client) current-issue-number)
                                       (const #f))
                                 (cons (var@@ (mumi client) issue-number-of-message)
                                       (const 12345))
                                 client-config-stub
                                 set-current-issue-stub)
             (cut (@@ (mumi client) send-email)
                  "mumi"
                  (list "foo.patch")))))))

(test-equal "send single patch to existing issue"
  '(("git" "send-email" "--to=12345@example.com"
     "--header-cmd=mumi git-send-email-headers"
     "foo.patch"))
  (map (match-lambda
         ((command _) command))
       (trace-calls (var@@ (mumi client) call-with-input-pipe*)
         (lambda ()
           (with-variables (list (cons (var@@ (mumi client) current-issue-number)
                                       (const 12345))
                                 (cons (var@@ (mumi client) reply-email-headers)
                                       (const '()))
                                 client-config-stub
                                 do-not-poll-server-for-issue-number)
             (cut (@@ (mumi client) send-email)
                  "mumi"
                  (list "foo.patch")))))))

(test-equal "send patch to existing issue and Cc other participants"
  '(("git" "send-email" "--to=12345@example.com"
     "--header-cmd=mumi git-send-email-headers"
     "foo.patch"))
  (map (match-lambda
         ((command _) command))
       (trace-calls (var@@ (mumi client) call-with-input-pipe*)
         (lambda ()
           (with-variables (list (cons (var@@ (mumi client) current-issue-number)
                                       (const 12345))
                                 (cons (var@@ (mumi client) reply-email-headers)
                                       (const `()))
                                 client-config-stub
                                 do-not-poll-server-for-issue-number)
             (cut (@@ (mumi client) send-email)
                  "mumi"
                  (list "foo.patch")))))))

(test-assert "parse-patch-subject with unlabelled v1 patch"
  (let ((patch (parse-patch-subject "[PATCH]")))
    (and (= (patch-reroll-count patch) 1)
         (= (patch-patch-index patch) 1)
         (= (patch-total-patches patch) 1))))

(test-assert "parse-patch-subject with second of unlabelled v1 patch series"
  (let ((patch (parse-patch-subject "[PATCH 2/7]")))
    (and (= (patch-reroll-count patch) 1)
         (= (patch-patch-index patch) 2)
         (= (patch-total-patches patch) 7))))

(test-assert "parse-patch-subject with single v2 patch"
  (let ((patch (parse-patch-subject "[PATCH v2]")))
    (and (= (patch-reroll-count patch) 2)
         (= (patch-patch-index patch) 1)
         (= (patch-total-patches patch) 1))))

(test-assert "parse-patch-subject with second of v2 patch series"
  (let ((patch (parse-patch-subject "[PATCH v2 2/3]")))
    (and (= (patch-reroll-count patch) 2)
         (= (patch-patch-index patch) 2)
         (= (patch-total-patches patch) 3))))

(test-end "client")
