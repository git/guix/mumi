# mumi - Mediocre, uh, mail interface
# Copyright © 2016-2020 Ricardo Wurmus <rekado@elephly.net>
# Copyright © 2025 Felix Lechner <felix.lechner@lease-up.com>
#
# This file is part of mumi.
#
# mumi is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# mumi is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mumi.  If not, see <http://www.gnu.org/licenses/>.

include guile.am

bin_SCRIPTS =						\
  scripts/mumi

moddir = $(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir  = $(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

dist_pkgdata_DATA = etc/schema.sql

SOURCES =							\
  mumi/web/download.scm				\
  mumi/web/server.scm				\
  mumi/web/render.scm				\
  mumi/web/controller.scm			\
  mumi/web/sxml.scm					\
  mumi/web/util.scm					\
  mumi/web/graphql.scm				\
  mumi/web/view/html.scm			\
  mumi/web/view/utils.scm			\
  mumi/cache.scm					\
  mumi/messages.scm					\
  mumi/jobs.scm						\
  mumi/send-email.scm				\
  mumi/client.scm				\
  mumi/config.scm					\
  mumi/debbugs.scm					\
  mumi/email.scm				\
  mumi/test-utils.scm				\
  mumi/issue.scm                                \
  mumi/xapian.scm

TEST_EXTENSIONS = .scm

SCM_TESTS = \
  tests/client.scm  \
  tests/debbugs.scm \
  tests/xapian.scm

TESTS = $(SCM_TESTS)

EXTRA_DIST += $(TESTS) \
  $(wildcard tests/data/*)

AM_TESTS_ENVIRONMENT = abs_top_srcdir="$(abs_top_srcdir)" GUILE_AUTO_COMPILE=0

SCM_LOG_DRIVER =                                          \
  $(GUILE) --no-auto-compile -L $(top_srcdir) -e main     \
      $(top_srcdir)/build-aux/test-driver.scm

AM_SCM_LOG_DRIVER_FLAGS = --brief=yes

PICO_SCSS = $(wildcard assets/pico/scss/*.scss)

assets/css/mumi.css: assets/mumi.scss assets/_theme-switcher.scss $(PICO_SCSS)
	$(SASSC) $< $@

nobase_nodist_pkgdata_DATA = \
  assets/css/mumi.css

nobase_dist_pkgdata_DATA = \
  assets/css/reset.css \
  assets/css/screen.css \
  assets/img/file.svg \
  assets/img/logo.png \
  assets/img/spin.gif \
  assets/js/mumi.js \
  assets/js/theme-switcher.js \
  assets/js/tokeninput.js

EXTRA_DIST +=					\
  pre-inst-env.in				\
  build-aux/test-driver.scm     \
  assets/mumi.scss \
  $(PICO_SCSS)
