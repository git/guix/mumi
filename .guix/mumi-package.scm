;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2023 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This file is part of mumi.
;;;
;;; mumi is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; mumi is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with mumi.  If not, see <http://www.gnu.org/licenses/>.

(define-module (mumi-package)
  #:use-module ((gnu packages guile-xyz) #:prefix guix:)
  #:use-module ((gnu packages mail) #:prefix guix:)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public mumi
  (package
    (inherit guix:mumi)
    (version (string-append (package-version guix:mumi) "-git"))
    (source (local-file ".."
                        "mumi-checkout"
                        #:recursive? #t
                        #:select? (or (git-predicate (dirname (current-source-directory)))
                                      (const #t))))))

mumi
