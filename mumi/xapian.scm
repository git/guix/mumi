;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2020, 2022 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2020, 2022–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi xapian)
  #:use-module (mumi config)
  #:use-module (mumi debbugs)
  #:use-module (mumi email)
  #:use-module ((mumi web util) #:select (msgid-hash))
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 threads)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (email email)
  #:use-module (xapian wrap)
  #:use-module (xapian xapian)
  #:use-module (rnrs bytevectors)
  #:export (bug-status
            bug-statuses
            index!
            index-files
            parse-search-query
            search-query
            search
            match-count))

(define (date< date1 date2)
  "Return #t if DATE1 is earlier than DATE2. Else, return #f."
  (< (time-second (date->time-monotonic date1))
     (time-second (date->time-monotonic date2))))

(define (parse-emails files)
  (n-par-map 4
             (lambda (file)
               (list file
                     ;; Read emails from bug log, and sort them earliest first.
                     ;; Filter out any invalid emails without a Date header
                     ;; (RFC5322 requires a Date header) so that they don't
                     ;; aggrieve us while sorting.
                     (sort (filter (lambda (email)
                                     (assq-ref (email-headers email)
                                               'date))
                                   (call-with-input-file file
                                     read-emails-from-bug-log))
                           (lambda (email1 email2)
                             (date< (assq-ref (email-headers email1)
                                              'date)
                                    (assq-ref (email-headers email2)
                                              'date))))))
             files))

(define (author email)
  (or (and=> (assoc-ref (email-headers email) 'from)
             (match-lambda
               ((from . rest)
                (format #f "~a ~a"
                        (assoc-ref from 'name)
                        (assoc-ref from 'address)))
               (from
                (format #f "~a ~a"
                        (assoc-ref from 'name)
                        (assoc-ref from 'address)))))
      "<unknown>"))

(define (bug-status->string bug)
  "Serialize BUG, a <bug-status> object, to a string."
  (call-with-output-string
    (cut write
         `((num . ,(bug-num bug))
           (package . ,(bug-package bug))
           (archived . ,(bug-archived bug))
           (blocked-by . ,(bug-blockedby bug))
           (blocks . ,(bug-blocks bug))
           (date . ,(time-second (date->time-monotonic (bug-date bug))))
           (done . ,(bug-done bug))
           (merged-with . ,(bug-mergedwith bug))
           (originator . ,(bug-originator bug))
           (owner . ,(bug-owner bug))
           (severity . ,(bug-severity bug))
           (subject . ,(bug-subject bug))
           (tags . ,(bug-tags bug)))
         <>)))

(define (string->bug-status str)
  "Deserialize STR into a <bug-status> object."
  (call-with-input-string str
    (lambda (port)
      (let ((bug-alist (read port)))
        (make-bug-status (assq-ref bug-alist 'num)
                         (assq-ref bug-alist 'package)
                         (assq-ref bug-alist 'archived)
                         (assq-ref bug-alist 'blocked-by)
                         (assq-ref bug-alist 'blocks)
                         (time-monotonic->date
                          (make-time time-monotonic
                                     0
                                     (assq-ref bug-alist 'date)))
                         (assq-ref bug-alist 'done)
                         (assq-ref bug-alist 'merged-with)
                         (assq-ref bug-alist 'originator)
                         (assq-ref bug-alist 'owner)
                         (assq-ref bug-alist 'severity)
                         (assq-ref bug-alist 'subject)
                         (assq-ref bug-alist 'tags))))))

(define (bug-status-from-xapian db bug-id)
  "Return <bug-status> object for BUG-ID from xapian DB."
  (let ((enq (enquire db (query (string-append "Q" (number->string bug-id))))))
    ;; Collapse on mergedwith value.
    (Enquire-set-collapse-key enq 2 1)
    (mset-fold (lambda (item _)
                 (string->bug-status (document-data (mset-item-document item))))
               #f
               (enquire-mset enq #:maximum-items 1))))

(define* (bug-status bug-id #:key (dbpath (string-append (%config 'db-dir)
                                                         "/mumi.xapian")))
  "Return <bug-status> object for BUG-ID from xapian index at DBPATH."
  (call-with-database dbpath
    (cut bug-status-from-xapian <> bug-id)))

(define* (bug-statuses bug-ids #:key (dbpath (string-append (%config 'db-dir)
                                                            "/mumi.xapian")))
  "Get <bug-status> objects for BUG-IDS from xapian index at DBPATH."
  (call-with-database dbpath
    (lambda (db)
      (map (cut bug-status-from-xapian db <>)
           bug-ids))))

(define* (index-files files #:key dbpath)
  "Parse all given Debbugs log files in FILES as a list of email
messages and index their contents in the Xapian database at DBPATH."
  (define (emails->file-tags emails)
    (let ((file-tags (map (match-lambda
                            ((tag . regexps)
                             (cons tag
                                   ;; Compile regexps into a single regexp.
                                   (make-regexp
                                    (string-append "(" (string-join regexps "|") ")")))))
                          (assq-ref (%server-config) 'file-tags))))
      ;; Build a list of file based tags. Deduplicate since different files may
      ;; map to the same tags.
      (delete-duplicates
       (append-map (lambda (file)
                     ;; Match file against tag regexps.
                     (filter-map (match-lambda
                                   ((tag . rx)
                                    (and (regexp-exec rx file)
                                         tag)))
                                 file-tags))
                   ;; Extract file names from patch emails. Deduplicate since
                   ;; the same file may have been modified in several different
                   ;; patches.
                   (delete-duplicates
                    (append-map (lambda (email)
                                  (let ((subject (assoc-ref (email-headers email)
                                                            'subject)))
                                    (if (and subject
                                             (string-prefix? "[PATCH" subject))
                                        (filter-map (lambda (line)
                                                      (and=> (string-match "[+-]{3} [ab]/"
                                                                           line)
                                                             match:suffix))
                                                    (string-split (email->text email)
                                                                  #\newline))
                                        (list))))
                                emails))))))

  (define (index-chunk files+mails)
    (call-with-writable-database
     dbpath
     (lambda (db)
       (for-each
        (match-lambda
          ((file (? pair? emails))
           (let* ((bugid (string->number (basename file ".log")))
                  (report (first emails))
                  (submitter (author report))
                  (subjects (delete-duplicates
                             (filter-map (lambda (email)
                                           (assoc-ref (email-headers email)
                                                      'subject))
                                         emails)))
                  (authors (delete-duplicates (map author emails)))
                  (date
                   (match (assoc-ref (email-headers report) 'date)
                     ((? string? s) #f)
                     (date date)))
                  (mdate
                   (match (assoc-ref (email-headers (last emails)) 'date)
                     ((? string? s) #f)
                     (date date)))
                  ;; Store the message ids as base64 strings of all
                  ;; messages.
                  (msgids (map msgid-hash
                               (filter-map (lambda (headers)
                                             (assoc-ref headers 'message-id))
                                           (map email-headers emails))))
                  (text (map email->text emails))

                  (bug (let ((bug (bug-status-from-summary bugid)))
                         (set-bug-tags bug
                                       (append (bug-tags bug)
                                               (emails->file-tags emails)))))
                  (idterm (string-append "Q" (number->string bugid)))
                  (doc (make-document #:data (bug-status->string bug)
                                      #:terms `((,idterm . 0)
                                                (,(string-append "XP" (bug-package bug)) . 0)
                                                (,(string-append "XS" (or (bug-severity bug)
                                                                          "normal"))
                                                 . 0)
                                                (,(string-append "XSTATUS" (cond
                                                                            ((bug-done bug) "done")
                                                                            (else "open")))
                                                 . 0)
                                                ,@(map (lambda (tag)
                                                         (cons (string-append "XT" (string-downcase tag))
                                                               0))
                                                       (bug-tags bug))
                                                ,@(map (lambda (msgid)
                                                         (cons (string-append "XU" msgid)
                                                               0))
                                                       msgids))
                                      #:values
                                      `((0 . ,(or (and date (date->string date "~Y~m~d")) "19700101"))
                                        (1 . ,(or (and mdate (date->string mdate "~Y~m~d")) "19700101"))
                                        ;; This is used for collapsing search results
                                        (2 . ,(string-join
                                               (sort (map number->string
                                                          (cons bugid
                                                                (bug-mergedwith bug)))
                                                     string<))))))
                  (term-generator (make-term-generator #:stem (make-stem "en")
                                                       #:document doc)))
             ;; Index fields with a suitable prefix. This allows for
             ;; searching separate fields as in subject:foo, from:bar,
             ;; etc. We do not keep track of the within document
             ;; frequencies of terms that will be used for boolean
             ;; filtering. We do not generate position information for
             ;; fields that will not need phrase searching or NEAR
             ;; searches.
             (index-text! term-generator
                          submitter
                          #:prefix "A"
                          #:wdf-increment 0)
             (for-each (cut index-text! term-generator <>
                            #:prefix "XA" #:wdf-increment 0)
                       authors)
             ;; TODO: This should only be the title of the bug, not
             ;; the subject.
             (for-each (cut index-text! term-generator <> #:prefix "S")
                       subjects)
             (index-text! term-generator
                          (or (bug-owner bug) "")
                          #:prefix "XO"
                          #:wdf-increment 0)

             ;; Index subject and body without prefixes for general
             ;; search.
             (for-each (lambda (subject)
                         (index-text! term-generator subject)
                         (increase-termpos! term-generator))
                       subjects)
             (for-each (lambda (text)
                         (index-text! term-generator text)
                         (increase-termpos! term-generator))
                       text)

             ;; Add the document to the database. The unique idterm
             ;; ensures each object ends up in the database only once
             ;; no matter how many times we run the indexer.
             (replace-document! db idterm doc)))
          (_ #f))                       ; ignore
        files+mails))))

  (let ((total (length files)))
    (let loop ((files files))
      (let-values (((chunk rest)
                    (if (>= (length files) 100)
                        (split-at files 100)
                        (values files '()))))
        (format (current-error-port)
                "indexing ~1,2f%~%"
                (exact->inexact (* 100 (/ (- total (length rest)) total))))
        (index-chunk (parse-emails chunk))
        (unless (null? rest)
          (loop rest))))))

(define (resolve-date-limit limit)
  (define now (current-time))
  (define (hours n)
    (make-time time-duration 0 (* 60 60 n)))
  (define (days n)
    (make-time time-duration 0 (* 60 60 24 n)))
  (let ((limit (string-delete #\- limit)))
    (cond
     ((and (string->number limit)
           (<= (string-length limit) 8))
      (format #f "~8,,,'0a" limit))
     (else
      (let ((date (match limit
                    ((or "now" "today") now)
                    ("yesterday" (subtract-duration now (days 1)))
                    (_
                     (call-with-values
                         (lambda () (span char-numeric? (string->list limit)))
                       (lambda (pre post)
                         (let ((n (string->number (apply string pre)))
                               (unit (apply string post)))
                           (and n
                                (subtract-duration
                                 now
                                 (match unit
                                   ((or "h" "hour" "hours")
                                    (hours n))
                                   ((or "d" "day" "days")
                                    (days n))
                                   ((or "w" "week" "weeks")
                                    (days (* n 7)))
                                   ((or "m" "month" "months")
                                    (days (* n 30)))
                                   ((or "y" "year" "years")
                                    (days (* n 365)))))))))))))
        (and date
             (date->string (time-utc->date date) "~Y~m~d")))))))

(define (make-date-range-processor slot prefix)
  (prefixed-range-processor
   slot
   (lambda (start stop)
     (DateRangeProcessor-apply
      (prefixed-date-range-processor slot #:prefix prefix)
      (if start
          (resolve-date-limit start)
          "")
      (if stop
          (resolve-date-limit stop)
          "")))
   #:prefix prefix))

(define parse-search-query
  ;; Parse query string passing a stemmer and suitable prefixes for field
  ;; search.
  (cut parse-query
       <>
       #:stemmer (make-stem "en")
       #:prefixes '(("title" . "S")
                    ("submitter" . "A")
                    ("author"    . "XA")
                    ("owner"     . "XO"))
       #:boolean-prefixes `(("package"   . "XP")
                            ("severity"  . "XS")
                            ("status"    . "XSTATUS")
                            ("tag"       . "XT")
                            ("is" . ,(field-processor
                                      (match-lambda
                                        ((or "done" "closed")
                                         (query "XSTATUSdone"))
                                        ((or "open" "pending")
                                         (query "XSTATUSopen")))))
                            ("msgid" . ,(field-processor
                                         (lambda (msgid)
                                           (query (string-append "XU"
                                                                 (msgid-hash msgid)))))))
       #:range-processors (list (make-date-range-processor 0 "date:")
                                (make-date-range-processor 1 "mdate:"))
       #:any-case-boolean? #t
       #:wildcard? #t))

(define* (search-query query #:key
                       (pagesize 1000)
                       (dbpath (string-append (%config 'db-dir) "/mumi.xapian")))
  "Search for QUERY, a Xapian Query object, in Xapian index at DBPATH. Return a
maximum of PAGESIZE results."
  ;; Open database for reading. call-with-database automatically closes the
  ;; database once we're done.
  (call-with-database dbpath
    (lambda (db)
      (let ((enq (enquire db query)))
        ;; Collapse on mergedwith value
        (Enquire-set-collapse-key enq 2 1)
        ;; Fold over the results, return bug id.
        (reverse
         (mset-fold (lambda (item acc)
                      (cons (string->bug-status
                             (document-data (mset-item-document item)))
                            acc))
                    '()
                    ;; Get an Enquire object from the database with the
                    ;; search results. Then, extract the MSet from the
                    ;; Enquire object.
                    (enquire-mset enq
                                  #:maximum-items pagesize)))))))

(define (search query-string . rest)
  (apply search-query
         (parse-search-query query-string)
         rest))

(define* (match-count query #:key (dbpath (string-append (%config 'db-dir) "/mumi.xapian")))
  "Return the number of matches for QUERY, a Xapian Query object, in Xapian index
at DBPATH."
  (call-with-database dbpath
    (lambda (db)
      (MSet-get-matches-estimated
       (enquire-mset (enquire db query)
                     #:maximum-items (database-document-count db))))))

(define* (index! #:key dbpath full?)
  "Index all Debbugs log files corresponding to the selected packages
into Xapian database at DBPATH.  When FULL? is #T process also
archived issues."
  (let* ((packages (assq-ref (%server-config) 'packages))
         (active-numbers (extract-bug-numbers packages))
         (archived-numbers (extract-bug-numbers packages #:archived? #t))
         (active-files (map bug-id->log-file active-numbers))
         (archived-files (map (cut bug-id->log-file <> #:archived? #t)
                              archived-numbers)))
    (index-files (if full?
                     (append active-files archived-files)
                     active-files)
                 #:dbpath dbpath)))
