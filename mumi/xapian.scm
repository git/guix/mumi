;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2020, 2022 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2020, 2022–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi xapian)
  #:use-module (mumi config)
  #:use-module (mumi debbugs)
  #:use-module (mumi email)
  #:use-module (mumi issue)
  #:use-module ((mumi web util) #:select (msgid-hash))
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 threads)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (email email)
  #:use-module (xapian wrap)
  #:use-module (xapian xapian)
  #:use-module (rnrs bytevectors)
  #:export (find-issue
            index!
            index-files
            parse-search-query
            search-query
            search
            match-count))

(define (alist-combine alist1 alist2)
  "Combine association lists ALIST1 and ALIST2. Pairs in ALIST1 get precedence over
those in ALIST2."
  (let ((keys1 (map (match-lambda
                      ((key . _) key))
                    alist1)))
    (append alist1
            (remove (match-lambda
                      ((key . _)
                       (member key keys1)))
                    alist2))))

(define (date< date1 date2)
  "Return #t if DATE1 is earlier than DATE2. Else, return #f."
  (< (time-second (date->time-monotonic date1))
     (time-second (date->time-monotonic date2))))

(define (parse-emails files)
  (n-par-map 4
             (lambda (file)
               (list file
                     ;; Read emails from bug log, and sort them earliest first.
                     ;; Filter out any invalid emails without a Date header
                     ;; (RFC5322 requires a Date header) so that they don't
                     ;; aggrieve us while sorting.
                     (sort (filter email-date
                                   (call-with-input-file file
                                     read-emails-from-bug-log))
                           (lambda (email1 email2)
                             (date< (email-date email1)
                                    (email-date email2))))))
             files))

(define (author email)
  (match (email-from email)
    (()
     "<unknown>")
    ((from _ ...)
     (format #f "~a ~a"
             (assq-ref from 'name)
             (assq-ref from 'address)))))

(define (issue->string issue)
  "Serialize ISSUE, an <issue> object, to a string."
  (call-with-output-string
    (cut write
         `((number . ,(issue-number issue))
           (package . ,(issue-package issue))
           (title . ,(issue-title issue))
           (date . ,(time-second (date->time-monotonic (issue-date issue))))
           (open? . ,(issue-open? issue))
           (submitter . ,(issue-submitter issue))
           (closer . ,(issue-closer issue))
           (owner . ,(issue-owner issue))
           (severity . ,(issue-severity issue))
           (tags . ,(issue-tags issue))
           (blocks . ,(issue-blocks issue))
           (blocked-by . ,(issue-blocked-by issue))
           (merged-with . ,(issue-merged-with issue))
           (archived . ,(issue-archived issue)))
         <>)))

(define (string->issue str mdate)
  "Deserialize document data STR with associated MDATE value into an <issue>
object."
  (call-with-input-string str
    (lambda (port)
      (let ((alist (read port)))
        (issue (assq-ref alist 'number)
               (assq-ref alist 'package)
               (assq-ref alist 'title)
               (time-monotonic->date
                (make-time time-monotonic
                           0
                           (assq-ref alist 'date)))
               (string->date mdate "~Y~m~d~H~M~S")
               (assq-ref alist 'open?)
               (assq-ref alist 'submitter)
               (assq-ref alist 'closer)
               (assq-ref alist 'owner)
               (assq-ref alist 'severity)
               (assq-ref alist 'tags)
               (assq-ref alist 'blocks)
               (assq-ref alist 'blocked-by)
               (assq-ref alist 'merged-with)
               (assq-ref alist 'archived))))))

(define (issue-from-xapian db bug-id)
  "Return <issue> object for BUG-ID from xapian DB."
  (let ((enq (enquire db (query (string-append "Q" (number->string bug-id))))))
    ;; Collapse on mergedwith value.
    (Enquire-set-collapse-key enq 2 1)
    (mset-fold (lambda (item _)
                 (let ((doc (mset-item-document item)))
                   (string->issue (document-data doc)
                                  (document-slot-ref doc 1))))
               #f
               (enquire-mset enq #:maximum-items 1))))

(define* (find-issue bug-id #:key (dbpath (string-append (%config 'db-dir)
                                                         "/mumi.xapian")))
  "Return <issue> object for BUG-ID from xapian index at DBPATH."
  (call-with-database dbpath
    (cut issue-from-xapian <> bug-id)))

(define* (index-files files #:key dbpath)
  "Parse all given Debbugs log files in FILES as a list of email
messages and index their contents in the Xapian database at DBPATH."
  (define mtimes
    ;; This may be the first time the xapian index is being built. So, check if
    ;; it exists.
    (if (file-exists? dbpath)
        (let ((mtime-str (call-with-database dbpath
                           (cut Database-get-metadata <> "mtimes"))))
          (if (string-null? mtime-str)
              '()
              (call-with-input-string mtime-str
                read)))
        '()))

  (define (emails->file-tags package-name emails)
    (let ((file-tags (map (match-lambda
                            ((tag . regexps)
                             (cons tag
                                   ;; Compile regexps into a single regexp.
                                   (make-regexp
                                    (string-append "(" (string-join regexps "|") ")")))))
                          (or (assq-ref (find-package package-name)
                                        'file-tags)
                              (list)))))
      ;; Build a list of file based tags. Deduplicate since different files may
      ;; map to the same tags.
      (delete-duplicates
       (append-map (lambda (file)
                     ;; Match file against tag regexps.
                     (filter-map (match-lambda
                                   ((tag . rx)
                                    (and (regexp-exec rx file)
                                         tag)))
                                 file-tags))
                   ;; Extract file names from patch emails. Deduplicate since
                   ;; the same file may have been modified in several different
                   ;; patches.
                   (delete-duplicates
                    (append-map (lambda (email)
                                  (let ((subject (email-subject email)))
                                    (if (and subject
                                             (string-prefix? "[PATCH" subject))
                                        (filter-map (lambda (line)
                                                      (and=> (string-match "[+-]{3} [ab]/"
                                                                           line)
                                                             match:suffix))
                                                    (string-split (email->text email)
                                                                  #\newline))
                                        (list))))
                                emails))))))

  (define (index-chunk files+mails)
    (call-with-writable-database
     dbpath
     (lambda (db)
       (for-each
        (match-lambda
          ((file (? pair? emails))
           (let* ((bugid (string->number (basename file ".log")))
                  (non-control-emails
                   (let ((control-email-address (assq-ref (%server-config)
                                                          'control-email-address)))
                     (remove (lambda (email)
                               (member control-email-address
                                       (map (cut assq-ref <> 'address)
                                            (append (email-to email)
                                                    (email-cc email)))))
                             emails)))
                  (report (first emails))
                  (submitter (author report))
                  (subjects (delete-duplicates
                             (filter-map email-subject
                                         emails)))
                  (authors (delete-duplicates (map author emails)))
                  (date (email-date report))
                  (mdate (email-date (last emails)))
                  ;; Store the message ids as base64 strings of all
                  ;; messages.
                  (msgids (map msgid-hash
                               (filter-map email-message-id emails)))
                  (text (map email->text emails))

                  (issue (let ((issue (bug->issue (bug-status-from-summary bugid))))
                           (set-issue-tags issue
                                           (append (emails->file-tags (issue-package issue)
                                                                      emails)
                                                   ;; Tag as unanswered if bug is
                                                   ;; open, and no one other than the
                                                   ;; original submitter have posted
                                                   ;; to it.
                                                   (if (and (issue-open? issue)
                                                            (match (delete-duplicates
                                                                    (map author non-control-emails))
                                                              ((submitter) #t)
                                                              (_ #f)))
                                                       (list "unanswered")
                                                       (list))))))
                  (idterm (string-append "Q" (number->string bugid)))
                  (doc (make-document #:data (issue->string issue)
                                      #:terms `((,idterm . 0)
                                                (,(string-append "XP" (issue-package issue))
                                                 . 0)
                                                (,(string-append "XS" (or (issue-severity issue)
                                                                          "normal"))
                                                 . 0)
                                                (,(string-append "XSTATUS"
                                                                 (if (issue-open? issue)
                                                                     "open"
                                                                     "done"))
                                                 . 0)
                                                ,@(map (lambda (tag)
                                                         (cons (string-append "XT" (string-downcase tag))
                                                               0))
                                                       (issue-tags issue))
                                                ,@(map (lambda (msgid)
                                                         (cons (string-append "XU" msgid)
                                                               0))
                                                       msgids))
                                      #:values
                                      `((0 . ,(date->string
                                               (or date
                                                   (time-monotonic->date (make-time time-monotonic 0 0)))
                                               "~Y~m~d~H~M~S"))
                                        (1 . ,(date->string
                                               (or mdate
                                                   (time-monotonic->date (make-time time-monotonic 0 0)))
                                               "~Y~m~d~H~M~S"))
                                        ;; This is used for collapsing search results
                                        (2 . ,(string-join
                                               (sort (map number->string
                                                          (cons bugid
                                                                (issue-merged-with issue)))
                                                     string<))))))
                  (term-generator (make-term-generator #:stem (make-stem "en")
                                                       #:document doc)))
             ;; Index fields with a suitable prefix. This allows for
             ;; searching separate fields as in subject:foo, from:bar,
             ;; etc. We do not keep track of the within document
             ;; frequencies of terms that will be used for boolean
             ;; filtering. We do not generate position information for
             ;; fields that will not need phrase searching or NEAR
             ;; searches.
             (index-text! term-generator
                          submitter
                          #:prefix "A"
                          #:wdf-increment 0)
             (for-each (cut index-text! term-generator <>
                            #:prefix "XA" #:wdf-increment 0)
                       authors)
             ;; TODO: This should only be the title of the bug, not
             ;; the subject.
             (for-each (cut index-text! term-generator <> #:prefix "S")
                       subjects)
             (index-text! term-generator
                          (or (issue-owner issue) "")
                          #:prefix "XO"
                          #:wdf-increment 0)

             ;; Index subject and body without prefixes for general
             ;; search.
             (for-each (lambda (subject)
                         (index-text! term-generator subject)
                         (increase-termpos! term-generator))
                       subjects)
             (for-each (lambda (text)
                         (index-text! term-generator text)
                         (increase-termpos! term-generator))
                       text)

             ;; Add the document to the database. The unique idterm
             ;; ensures each object ends up in the database only once
             ;; no matter how many times we run the indexer.
             (replace-document! db idterm doc)))
          (_ #f))                       ; ignore
        files+mails))))

  (let* ((modified-files
          ;; See https://apenwarr.ca/log/20181113 for the many pitfalls of mtime
          ;; comparison.
          (remove (lambda (file)
                    (eqv? (assv-ref mtimes (string->number (basename file ".log")))
                          (stat:mtime (stat file))))
                  files))
         (new-mtimes (alist-combine (map (lambda (file)
                                           (cons (string->number (basename file ".log"))
                                                 (stat:mtime (stat file))))
                                         modified-files)
                                    mtimes))
         (total (length modified-files)))
    (format (current-error-port)
            "Indexing ~a modified issues~%"
            total)
    (let loop ((files modified-files))
      (let-values (((chunk rest)
                    (if (>= (length files) 100)
                        (split-at files 100)
                        (values files '()))))
        (format (current-error-port)
                "indexing ~1,2f%~%"
                (exact->inexact (* 100 (if (zero? total)
                                           1
                                           (/ (- total (length rest)) total)))))
        (index-chunk (parse-emails chunk))
        (unless (null? rest)
          (loop rest))))
    (call-with-writable-database dbpath
      (lambda (db)
        (WritableDatabase-set-metadata db "mtimes"
                                       (call-with-output-string
                                         (cut write new-mtimes <>)))))))

(define (resolve-date-limit limit)
  (define now (current-time))
  (define (hours n)
    (make-time time-duration 0 (* 60 60 n)))
  (define (days n)
    (make-time time-duration 0 (* 60 60 24 n)))
  (let ((limit (string-delete #\- limit)))
    (cond
     ((and (string->number limit)
           (<= (string-length limit) 8))
      (format #f "~8,,,'0a" limit))
     (else
      (let ((date (match limit
                    ((or "now" "today") now)
                    ("yesterday" (subtract-duration now (days 1)))
                    (_
                     (call-with-values
                         (lambda () (span char-numeric? (string->list limit)))
                       (lambda (pre post)
                         (let ((n (string->number (apply string pre)))
                               (unit (apply string post)))
                           (and n
                                (subtract-duration
                                 now
                                 (match unit
                                   ((or "h" "hour" "hours")
                                    (hours n))
                                   ((or "d" "day" "days")
                                    (days n))
                                   ((or "w" "week" "weeks")
                                    (days (* n 7)))
                                   ((or "m" "month" "months")
                                    (days (* n 30)))
                                   ((or "y" "year" "years")
                                    (days (* n 365)))))))))))))
        (and date
             (date->string (time-utc->date date) "~Y~m~d")))))))

(define (make-date-range-processor slot prefix)
  (prefixed-range-processor
   slot
   (lambda (start stop)
     (DateRangeProcessor-apply
      (prefixed-date-range-processor slot #:prefix prefix)
      (if start
          (resolve-date-limit start)
          "")
      (if stop
          (resolve-date-limit stop)
          "")))
   #:prefix prefix))

(define parse-search-query
  ;; Parse query string passing a stemmer and suitable prefixes for field
  ;; search.
  (cut parse-query
       <>
       #:stemmer (make-stem "en")
       #:prefixes '(("title" . "S")
                    ("submitter" . "A")
                    ("author"    . "XA")
                    ("owner"     . "XO"))
       #:boolean-prefixes `(("package"   . "XP")
                            ("severity"  . "XS")
                            ("status"    . "XSTATUS")
                            ("tag"       . "XT")
                            ("is" . ,(field-processor
                                      (match-lambda
                                        ((or "done" "closed")
                                         (query "XSTATUSdone"))
                                        ((or "open" "pending")
                                         (query "XSTATUSopen")))))
                            ("msgid" . ,(field-processor
                                         (lambda (msgid)
                                           (query (string-append "XU"
                                                                 (msgid-hash msgid)))))))
       #:range-processors (list (make-date-range-processor 0 "date:")
                                (make-date-range-processor 1 "mdate:"))
       #:any-case-boolean? #t
       #:wildcard? #t))

(define* (search-query query #:key
                       (pagesize 1000)
                       (dbpath (string-append (%config 'db-dir) "/mumi.xapian")))
  "Search for QUERY, a Xapian Query object, in Xapian index at DBPATH. Return a
maximum of PAGESIZE results."
  ;; Open database for reading. call-with-database automatically closes the
  ;; database once we're done.
  (call-with-database dbpath
    (lambda (db)
      (let ((enq (enquire db query)))
        ;; Sort by relevance, then by mdate. We sort by mdate so that results
        ;; for strictly boolean queries (such as mdate:10d..) are sorted
        ;; meaningfully. If we didn't, results would be sorted in some arbitrary
        ;; order, such as by bug number.
        (Enquire-set-sort-by-relevance-then-value enq 1 #t)
        ;; Collapse on mergedwith value
        (Enquire-set-collapse-key enq 2 1)
        ;; Fold over the results, return bug id.
        (reverse
         (mset-fold (lambda (item acc)
                      (cons (let ((doc (mset-item-document item)))
                              (string->issue (document-data doc)
                                             (document-slot-ref doc 1)))
                            acc))
                    '()
                    ;; Get an Enquire object from the database with the
                    ;; search results. Then, extract the MSet from the
                    ;; Enquire object.
                    (enquire-mset enq
                                  #:maximum-items pagesize)))))))

(define (search query-string . rest)
  (apply search-query
         (parse-search-query query-string)
         rest))

(define* (match-count query #:key (dbpath (string-append (%config 'db-dir) "/mumi.xapian")))
  "Return the number of matches for QUERY, a Xapian Query object, in Xapian index
at DBPATH."
  (call-with-database dbpath
    (lambda (db)
      (MSet-get-matches-estimated
       (enquire-mset (enquire db query)
                     #:maximum-items (database-document-count db))))))

(define* (index! #:key dbpath)
  "Index all Debbugs log files corresponding to the selected packages
into Xapian database at DBPATH."
  (let* ((packages (append-map (match-lambda
                                 ((_ . configuration)
                                  (assq-ref configuration 'debbugs-packages)))
                               (assq-ref (%server-config) 'packages)))
         (active-numbers (extract-bug-numbers packages))
         (archived-numbers (extract-bug-numbers packages #:archived? #t))
         (active-files (map bug-id->log-file active-numbers))
         (archived-files (map (cut bug-id->log-file <> #:archived? #t)
                              archived-numbers)))
    (index-files (append active-files archived-files)
                 #:dbpath dbpath)))
