;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi issue)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 match)
  #:use-module (mumi config)
  #:use-module (mumi debbugs)
  #:export (issue
            issue-number
            issue-package
            issue-title
            issue-date
            issue-last-updated
            issue-open?
            issue-submitter
            issue-closer
            issue-owner
            issue-severity
            issue-tags
            set-issue-tags
            issue-blocks
            issue-blocked-by
            issue-merged-with
            issue-archived

            bug->issue))

(define-immutable-record-type <issue>
  (issue number package title date last-updated open? submitter closer owner
         severity tags blocks blocked-by merged-with archived)
  issue?
  (number issue-number)
  (package issue-package)
  (title issue-title)
  (date issue-date)
  (last-updated issue-last-updated)
  (open? issue-open?)
  (submitter issue-submitter)
  (closer issue-closer)
  (owner issue-owner)
  (severity issue-severity)
  (tags issue-tags set-issue-tags)
  (blocks issue-blocks)
  (blocked-by issue-blocked-by)
  (merged-with issue-merged-with)
  (archived issue-archived))

(define (bug->issue bug)
  "Convert BUG, a <bug-status> object, to an <issue> object."
  (issue (bug-num bug)
         (any (match-lambda
                ((aggregated-package . configuration)
                 (and (member (bug-package bug)
                              (assq-ref configuration 'debbugs-packages))
                      aggregated-package)))
              (assq-ref (%server-config) 'packages))
         (bug-subject bug)
         (bug-date bug)
         #f
         (not (bug-done bug))
         (bug-originator bug)
         (bug-done bug)
         (bug-owner bug)
         (bug-severity bug)
         (bug-tags bug)
         (bug-blocks bug)
         (bug-blockedby bug)
         (bug-mergedwith bug)
         (bug-archived bug)))
