;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2022, 2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi cache)
  #:use-module (mumi config)
  #:use-module (ice-9 match)
  #:export (cached? cache! forget! forget-all!))

(define %cache (make-hash-table))

(define (cached? key)
  "Return the value matching KEY from the cache if it has not yet
expired or return #F."
  (let ((t (current-time)))
    (match (hash-ref %cache key)
      ((#:expires time #:value value)
       (if (< t time) value #f))
      (_ #f))))

(define* (cache! key value
                 #:optional (ttl (assq-ref (%server-config) 'cache-ttl)))
  "Store VALUE for the given KEY and mark it to expire after TTL
seconds. Return VALUE."
  (let ((t (current-time)))
    (hash-set! %cache key `(#:expires ,(+ t ttl) #:value ,value))
    value))

(define (forget! key)
  "Delete KEY from the cache."
  (hash-remove! %cache key))

(define (forget-all!)
  "Reset the cache."
  (set! %cache (make-hash-table)))
