;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2023–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This file is part of mumi.
;;;
;;; mumi is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; mumi is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with mumi.  If not, see <http://www.gnu.org/licenses/>.

(define-module (mumi client)
  #:use-module (rnrs exceptions)
  #:use-module (rnrs io ports)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-43)
  #:use-module (srfi srfi-71)
  #:use-module (srfi srfi-171)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 regex)
  #:use-module (term ansi-color)
  #:use-module (web client)
  #:use-module (web response)
  #:use-module (web uri)
  #:use-module (email email)
  #:use-module (kolam http)
  #:use-module (mumi config)
  #:use-module (mumi email)
  #:use-module (mumi web view utils)
  #:export (search
            print-current-issue
            set-current-issue!
            clear-current-issue!
            www
            send-email
            git-send-email-headers
            compose-email
            command-email
            am))

(define (git-top-level)
  "Return the top-level directory of the current git repository."
  (let loop ((curdir (getcwd)))
    (cond
     ((file-exists? (string-append curdir "/.git"))
      curdir)
     ((string=? curdir "/")
      (error "No git top level found"))
     (else
      (loop (dirname curdir))))))

(define (client-config-directory)
  "Return client configuration directory."
  (string-append (git-top-level) "/.mumi"))

(define (client-config key)
  "Return client configuration value corresponding to KEY."
  (or (assq-ref (call-with-input-file (string-append (client-config-directory)
                                                     "/config")
                  read)
                key)
      (case key
        ((mumi-scheme) 'https)
        ((mumi-port) #f)
        ((control-email-address) "control@debbugs.gnu.org")
        (else
         (format (current-error-port)
                 "Key '~a not configured for mumi client.~%"
                 key)
         (exit #f)))))

(define (build-mumi-uri path)
  "Build URI to mumi with PATH."
  (build-uri (client-config 'mumi-scheme)
             #:host (client-config 'mumi-host)
             #:port (client-config 'mumi-port)
             #:path path))

(define (mumi-graphql-get document)
  "Make GraphQL GET query with DOCUMENT."
  (graphql-http-get (uri->string (build-mumi-uri "/graphql"))
                    document))

(define (iso8601->date str)
  "Convert ISO-8601 date/time+zone string to date object."
  (string->date str "~Y-~m-~dT~H:~M:~S~z"))

(define (list-issue issue)
  "List issue described by ISSUE association list."
  (display (colorize-string
            (number->string (assoc-ref issue "number"))
            'YELLOW))
  (display " ")
  (unless (assoc-ref issue "open")
    (display (colorize-string "✓" 'BOLD 'GREEN))
    (display " "))
  (display (colorize-string
            (assoc-ref issue "title")
            'MAGENTA 'UNDERLINE))
  ;; Print severity unless it is "normal".
  (match (assoc-ref issue "severity")
    ("normal" #t)
    (severity
     (display " ")
     (display (colorize-string severity
                               (match severity
                                 ((or "critical" "grave" "serious" "important")
                                  'ON-RED)
                                 ((or "minor" "wishlist")
                                  'ON-GREEN))))))
  (match (assoc-ref issue "tags")
    (#() #t)
    (#(tags ...)
     (display " ")
     (display (string-join (map (cut colorize-string <> 'ON-BLUE)
                                tags)))))
  (newline)
  (display (string-append
            "opened "
            (colorize-string (time->string
                              (iso8601->date (assoc-ref issue "date")))
                             'CYAN)
            " by "
            (colorize-string
             (let ((submitter (assoc-ref issue "submitter")))
               (if (eq? (assoc-ref submitter "name") 'null)
                   (assoc-ref submitter "address")
                   (assoc-ref submitter "name")))
             'CYAN)
            ", last updated "
            (colorize-string (time->string
                              (iso8601->date (assoc-ref issue "last_updated")))
                             'CYAN)))
  (newline))

(define (search query)
  "Search for issues with QUERY and list results."
  (vector-for-each (lambda (_ issue)
                     (list-issue issue))
                   (assoc-ref
                    (mumi-graphql-get
                     `(document
                       (query (#(issues #:search ,query)
                               number
                               title
                               open
                               date
                               last_updated
                               severity
                               tags
                               (submitter name address)))))
                    "issues")))

(define (current-issue-file)
  "Return path to current issue number file."
  (string-append (client-config-directory) "/current-issue"))

(define (current-issue-number)
  "Return current issue number."
  (let ((issue-file (current-issue-file)))
    (and (file-exists? issue-file)
         (call-with-input-file issue-file
           read))))

(define (print-current-issue)
  "Print current issue."
  (let ((issue-number (current-issue-number)))
    (if issue-number
        (list-issue
         (assoc-ref
          (mumi-graphql-get
           `(document
             (query (#(issue #:number ,issue-number)
                     number
                     title
                     open
                     date
                     last_updated
                     severity
                     tags
                     (submitter name address)))))
          "issue"))
        (begin
          (format (current-error-port) "No current issue!~%")
          (exit #f)))))

(define (set-current-issue! issue-number)
  "Set current issue number."
  ;; TODO: Write file atomically.
  (call-with-output-file (current-issue-file)
    (cut write issue-number <>)))

(define (clear-current-issue!)
  "Clear current issue."
  (let ((issue-file (current-issue-file)))
    (when (file-exists? issue-file)
      (delete-file issue-file))))

(define* (issue-number-of-message message-id #:optional (retries 15))
  "Return issue number that MESSAGE-ID belongs to. Retry RETRIES number
of times with an interval of 60 seconds between retries."
  ;; TODO: Re-implement this using our GraphQL endpoint once it
  ;; supports retrieving the issue from a message ID. Later,
  ;; re-implement this using a GraphQL subscription when kolam
  ;; supports it.
  (define (poll-issue-number-of-message message-id)
    (let ((response _ (http-get (build-mumi-uri (string-append "/msgid/" message-id)))))
      (and (>= (response-code response) 300)
           (< (response-code response) 400)
           (match (split-and-decode-uri-path
                   (uri-path (response-location response)))
             (("issue" issue-number)
              (string->number issue-number))))))

  (let loop ((i retries))
    (if (zero? i)
        (begin
          (format (current-error-port)
                  "Mail not acknowledged by issue tracker. Giving up.~%")
          (exit #f))
        (or (poll-issue-number-of-message message-id)
            (begin
              (let ((retry-interval 60))
                (format (current-error-port)
                        "Server has not yet received our email. Will retry in ~a seconds. ~a retries remaining.~%"
                        retry-interval (1- i))
                (sleep retry-interval))
              (loop (1- i)))))))

(define (call-with-input-pipe command proc)
  "Call PROC with input pipe to COMMAND, a string."
  (let ((port #f))
    (dynamic-wind
      (lambda ()
        (set! port (open-input-pipe command)))
      (cut proc port)
      (lambda ()
        (unless (zero? (close-pipe port))
          (error "Command invocation failed" command))))))

(define (call-with-input-pipe* command proc)
  "Call PROC with input pipe to COMMAND. COMMAND is a list of program
arguments."
  (match command
    ((prog args ...)
     (let ((port #f))
       (dynamic-wind
         (lambda ()
           (set! port (apply open-pipe* OPEN_READ prog args)))
         (cut proc port)
         (lambda ()
           (unless (zero? (close-pipe port))
             (error "Command invocation failed" command))))))))

(define (call-with-output-pipe* proc program . args)
  "Execute PROGRAM ARGS ... in a subprocess with a pipe to it. Call PROC
with an output port to that pipe. Close the pipe once PROC exits, even
if it exits non-locally. Return the value returned by PROC."
  (let ((port #f))
    (dynamic-wind
      (cut set! port (apply open-pipe* OPEN_WRITE program args))
      (cut proc port)
      (lambda ()
        (let ((return-value (status:exit-val (close-pipe port))))
          (unless (and return-value
                       (zero? return-value))
            (error "Command invocation failed" (cons program args))))))))

(define (serialize-email-address name address)
  "Combine NAME and ADDRESS into a complete email address of the form
\"NAME <ADDRESS>\". Double quote NAME if necessary. NAME may be #f."
  (if name
      (string-append (if (string-contains name ",")
                         (string-append "\"" name "\"")
                         name)
                     " <" address ">")
      address))

(define (split-cc cc)
  "Split CC into a list of email addresses."
  (map (lambda (address)
         (serialize-email-address (assq-ref address 'name)
                                  (assq-ref address 'address)))
       (assq-ref (parse-email-headers (string-append "Cc: " cc "\n"))
                 'cc)))

(define (invoke . args)
  "Invoke program specified by ARGS. Raise exception if it fails."
  (unless (zero? (apply system* args))
    (error "Command invocation failed" args)))

(define (www)
  "Open current issue in a web browser."
  (let ((issue-number (current-issue-number)))
    (if issue-number
        (invoke (%config 'xdg-open)
                (uri->string
                 (build-mumi-uri (string-append "/" (number->string issue-number)))))
        (begin
          (format (current-error-port) "No current issue!~%")
          (exit #f)))))

(define* (git-send-email to patches #:optional (options '()))
  "Send PATCHES using git send-email to the TO address with
OPTIONS. Return the message ID of the first email sent."
  (let ((command (cons* "git" "send-email"
                        (string-append "--to=" to)
                        (append options
                                patches))))
    (display (string-join command))
    (newline)
    (call-with-input-pipe* command
      (lambda (port)
        ;; FIXME: This messes up the order of stdout and stderr.
        (let ((message-id
               ;; Read till you get the Message ID.
               (port-transduce (tlog (lambda (_ line)
                                       (display line)
                                       (newline)))
                               (rany (lambda (line)
                                       (and (string-prefix-ci? "Message-ID:" line)
                                            (assq-ref
                                             (parse-email-headers
                                              (string-append line "\n"))
                                             'message-id))))
                               get-line
                               port)))
          ;; Pass through the rest.
          (display (get-string-all port))
          message-id)))))

;; TODO: This is essentially a null detection function. Build it into
;; kolam itself.
(define (person-name-ref person)
  "Return name from a PERSON association list returned from a mumi
GraphQL query."
  (case (assoc-ref person "name")
    ((null) #f)
    (else => identity)))

(define (quote-text text)
  "Quote TEXT by prefixing each line with a >."
  (call-with-output-string
    (lambda (port)
      ;; Leave a blank line before quoted text.
      (newline port)
      (for-each (lambda (line)
                  (display "> " port)
                  ;; Deal with CRLF line endings if present.
                  (display (string-trim-right line (char-set #\return))
                           port)
                  (newline port))
                (string-split text #\newline)))))

(define* (reply-email-headers issue-number #:key reply-to include-body?)
  "Return an association list of email headers when replying to
the REPLY-TO th message of ISSUE-NUMBER. When REPLY-TO is #f, assume
the reply is to the last message. When INCLUDE-BODY? is #t, include
quoted body in the returned association list."
  (let* ((issue
          (assoc-ref
           (mumi-graphql-get
            `(document
              (query (#(issue #:number ,issue-number)
                      title
                      (messages (from name address)
                                date
                                message_id)))))
           "issue"))
         (messages (vector->list (assoc-ref issue "messages"))))
    `((subject . ,(string-append "Re: " (assoc-ref issue "title")))
      ;; When reply-to is not specified, reply to the last message.
      (in-reply-to . ,(string-append "<"
                                     (assoc-ref (if reply-to
                                                    (list-ref messages reply-to)
                                                    (last messages))
                                                "message_id")
                                     ">"))
      ;; Cc all issue participants.
      ,@(match (delete-duplicates
                (map (lambda (message)
                       (let ((from (assoc-ref message "from")))
                         (serialize-email-address (person-name-ref from)
                                                  (assoc-ref from "address"))))
                     messages))
          (() (list))
          (cc (list (cons 'cc (string-join cc ", ")))))
      ,@(if include-body?
            `((body . ,(quote-text
                        (email->text
                         (let ((_ port (http-get (build-mumi-uri
                                                  (format #f "/issue/~a/raw/~a"
                                                          issue-number
                                                          (or reply-to
                                                              (1- (length messages)))))
                                                 #:streaming? #t)))
                           (parse-email (first (mbox->emails port))))))))
            (list)))))

(define (send-email mumi-command patches)
  "Send PATCHES via email. MUMI-COMMAND is the mumi program currently
invoked."
  (if (current-issue-number)
      ;; If an issue is current, send patches to that issue's email
      ;; address.
      (let ((issue-number (current-issue-number)))
        (git-send-email (string-append (number->string issue-number)
                                       "@"
                                       (client-config 'debbugs-host))
                        patches
                        (list (string-append "--header-cmd="
                                             mumi-command
                                             " git-send-email-headers"))))
      ;; New issue; do the Debbugs dance.
      (match patches
        ((first-patch other-patches ...)
         ;; Send first patch to the patch email address and get an issue number.
         (let ((issue-number (issue-number-of-message
                              (git-send-email (client-config 'patch-email-address)
                                              (list first-patch)))))
           ;; Send the remaining patches, if any, to that issue's email address.
           (unless (null? other-patches)
             (git-send-email
              (string-append (number->string issue-number)
                             "@"
                             (client-config 'debbugs-host))
              other-patches))
           ;; Set the current issue number.
           (set-current-issue! issue-number))))))

(define (git-send-email-headers patch)
  "Print send-email headers for PATCH."
  (let* (;; Compute headers if configured in git config.
         (header-command
          (false-if-exception
            (call-with-input-pipe* (list "git" "config" "sendemail.headerCmd")
              get-line)))
         (headers
          (if header-command
              ;; sendemail.headerCmd is only available to us as a
              ;; string, and not as a list of arguments. It is quite
              ;; non-trivial to correctly split the string back into a
              ;; list of arguments. That would require correct
              ;; handling of quotes like the shell does. So, we use
              ;; call-with-input-pipe instead of
              ;; call-with-input-pipe*.
              (call-with-input-pipe (string-append header-command " " patch)
                get-string-all)
              ""))
         (external-x-debbugs-cc
          (cond
           ((assq-ref (parse-email-headers (string-append headers "\n"))
                       'x-debbugs-cc)
            => split-cc)
           (else '())))
         ;; Fetch Cc addresses for current issue.
         (x-debbugs-cc
          (cond
           ((assq-ref (reply-email-headers (current-issue-number))
                       'cc)
            => split-cc)
           (else '()))))
    ;; Print X-Debbugs-Cc header.
    (display "X-Debbugs-Cc: ")
    (display (string-join (delete-duplicates
                           (append x-debbugs-cc external-x-debbugs-cc))
                          ", "))
    (newline)
    ;; Print headers other than X-Debbugs-Cc.
    ;; TODO: RFC5322 headers are not restricted to a single
    ;; line. "Folded" multi-line headers are allowed. Support them.
    (for-each (lambda (line)
                (unless (string-prefix-ci? "X-Debbugs-Cc:" line)
                  (display line)
                  (newline)))
              (string-split headers #\newline))))

(define* (mailto-uri to #:optional (headers '()))
  "Construct mailto URI for TO address with HEADERS."
  (string-append "mailto:" to "?"
                 (string-join (map (match-lambda
                                     ((header . value)
                                      (string-append (symbol->string header)
                                                     "="
                                                     (uri-encode value))))
                                   headers)
                              "&")))

(define* (compose-email #:key close? reply-to-spec)
  "Compose email message. When CLOSE? is #t, write to the -done email
address of the issue. REPLY-TO-SPEC is a message spec indicating the
message to reply to. If unspecified, assume this is a reply to the
last message."
  (invoke (%config 'xdg-open)
          (let ((issue-number (current-issue-number)))
            (if issue-number
                (let ((reply-to (cond
                                 ((not reply-to-spec) #f)
                                 ((string-prefix? "@" reply-to-spec)
                                  (string->number
                                   (string-drop reply-to-spec 1)))
                                 (else
                                  (format (current-error-port)
                                          "Unrecognized message spec ~a~%"
                                          reply-to-spec)
                                  (exit #f)))))
                  ;; If an issue is current, fill in the headers relevant
                  ;; to that issue and compose to that issue's email
                  ;; address.
                  (mailto-uri (string-append (number->string issue-number)
                                             (if close? "-done" "")
                                             "@"
                                             (client-config 'debbugs-host))
                              (reply-email-headers issue-number
                                                   #:reply-to reply-to
                                                   #:include-body? #t)))
                ;; Else, send to the new patch email address. After
                ;; sending this email, we would also like to set the
                ;; current issue number. But, the user's email client
                ;; is outside our control and there is probably no way
                ;; to find the issue number that was created.
                (begin
                  (when close?
                    (begin
                      (format (current-error-port) "No current issue to close!~%")
                      (exit #f)))
                  (mailto-uri (client-config 'patch-email-address)))))))

(define* (command-email #:key reassign severity (merge '()) (tags '()))
  "Compose command email message. When REASSIGN is not #f, reassign the current
issue to the package name specified as REASSIGN. When SEVERITY is not #f, set
the severity of the current issue to SEVERITY. MERGE is a list of issue numbers
to merge the current issue with. TAGS is a list of tags to tag the current issue
with. TAGS is a list of strings, each one of which starts with a + or - prefix."
  ;; Error out if there is no current issue.
  (unless (current-issue-number)
    (format (current-error-port) "No current issue!~%")
    (exit #f))

  (define merge-lines
    (match-lambda
      (() '())
      (merge
       (let ((merge-titles
              (map (match-lambda
                     (("issue" ("title" . title))
                      title))
                   (mumi-graphql-get
                    `(document
                      ,@(map (lambda (issue-number)
                               `(query (#(issue #:number ,issue-number)
                                        title)))
                             merge))))))
         (append (map (cut format #f "# Merge with ~s" <>)
                      merge-titles)
                 (list (format #f "merge ~a ~a"
                               (current-issue-number)
                               (string-join (map number->string merge)))))))))

  (define (severity-lines severity)
    (cond
     ((not severity) '())
     ((member severity (list "critical" "grave" "serious" "important"
                             "normal" "minor" "wishlist"))
      (list (format #f "severity ~a ~a"
                      (current-issue-number)
                      severity)))
     (else
      (format (current-error-port)
              "Severity must be one of critical, grave, serious, important,
normal, minor or wishlist.

See https://debbugs.gnu.org/Developer.html#severities for their accepted
meanings.~%")
      (exit #f))))

  (define (tag-lines tags)
    (let ((tag-additions tag-removals (partition (cut string-prefix? "+" <>)
                                                 tags)))
      (append (match tag-additions
                (() '())
                (_ (list (format #f "tags ~a + ~a"
                                 (current-issue-number)
                                 (string-join (map (cut string-drop <> 1)
                                                   tag-additions))))))
              (match tag-removals
                (() '())
                (_ (list (format #f "tags ~a - ~a"
                                 (current-issue-number)
                                 (string-join (map (cut string-drop <> 1)
                                                   tag-removals)))))))))

  (let ((lines (append (if reassign
                           (list (format #f "reassign ~a ~a"
                                         (current-issue-number)
                                         reassign))
                           '())
                       (severity-lines severity)
                       (merge-lines merge)
                       (tag-lines tags)
                       (list "thanks"))))
    (invoke (%config 'xdg-open)
            (mailto-uri (client-config 'control-email-address)
                        `((subject . ,(format #f "Manipulate bug ~a"
                                              (current-issue-number)))
                          (body . ,(string-join (cons "" lines)
                                                "\n"
                                                'suffix)))))))

(define-immutable-record-type <patch>
  (patch subject reroll-count patch-index total-patches message-index)
  patch?
  (subject patch-subject)
  (reroll-count patch-reroll-count)
  (patch-index patch-patch-index)
  (total-patches patch-total-patches)
  (message-index patch-message-index set-patch-message-index))

(define (parse-patch-subject subject)
  "Parse patch SUBJECT and return a <patch> object. Return #f if SUBJECT
is not that of a patch."
  (let ((m (string-match "^\\[PATCH( v([[:digit:]]*)){0,1}( ([[:digit:]]*)/([[:digit:]]*)){0,1}\\]"
                         subject)))
    (and m
         (patch subject
                (or (and (match:substring m 2)
                         (string->number (match:substring m 2)))
                    1)
                (or (and (match:substring m 4)
                         (string->number (match:substring m 4)))
                    1)
                (or (and (match:substring m 5)
                         (string->number (match:substring m 5)))
                    1)
                #f))))

(define* (am #:key spec dry-run? git-am-args)
  "Apply patches referred to SPEC. SPEC may be a string describing a
reroll count or a message index. If SPEC is not specified, find and
apply the latest patchset. When DRY-RUN? is #t, print out which
patches will be applied; don't actually apply them. Pass GIT-AM-ARGS
to git am."
  (cond
   ;; Error out since there is no current issue.
   ((not (current-issue-number))
    (format (current-error-port) "No current issue!~%")
    (exit #f))
   ;; Find and apply the latest patchset.
   ((not spec)
    (am-reroll-count #:dry-run? dry-run?
                     #:git-am-args git-am-args))
   ;; Apply patchset with a specified reroll count.
   ((string-prefix? "v" spec)
    (am-reroll-count #:reroll-count (string->number (string-drop spec 1))
                     #:dry-run? dry-run?
                     #:git-am-args git-am-args))
   ;; Apply a specific message alone.
   ((string-prefix? "@" spec)
    (am-message-index (string->number (string-drop spec 1))
                      #:dry-run? dry-run?
                      #:git-am-args git-am-args))
   (else
    (format (current-error-port) "Unrecognized patchset spec ~a~%" spec)
    (exit #f))))

;; TODO: This is essentially a null detection function. Build it into
;; kolam itself.
(define (message-subject-ref message)
  "Return subject from a MESSAGE association list returned from a mumi
GraphQL query."
  (case (assoc-ref message "subject")
    ((null) #f)
    (else => identity)))

(define* (am-message-index index #:key dry-run? git-am-args)
  "Apply patch from INDEXth message. When DRY-RUN? is #t, print out
which patch will be applied; don't actually apply it. Pass GIT-AM-ARGS
to git am."
  (let* ((issue-number (current-issue-number))
         (messages (assoc-ref
                    (assoc-ref (mumi-graphql-get
                                `(document
                                  (query (#(issue #:number ,issue-number)
                                          (messages
                                           subject)))))
                               "issue")
                    "messages")))
    (unless (< index (vector-length messages))
      (format (current-error-port) "Message @~a does not exist~%" index)
      (exit #f))
    (if dry-run?
        (format #t "Apply @~a ~a~%"
                index
                (colorize-string (message-subject-ref (vector-ref messages index))
                                 'MAGENTA))
        (let ((_ mbox (http-get (build-mumi-uri (format #f "/issue/~a/raw/~a"
                                                        issue-number
                                                        index)))))
          (apply call-with-output-pipe*
                 (cut put-bytevector <> mbox)
                 "git" "am" git-am-args)))))

(define* (am-reroll-count #:key reroll-count dry-run? git-am-args)
  "Apply patches with REROLL-COUNT. If REROLL-COUNT is not specified,
find and apply the latest patchset. When DRY-RUN? is #t, print out
which patches will be applied; don't actually apply them. Pass
GIT-AM-ARGS to git am."
  (let ((issue-number (current-issue-number)))
    (let* ((messages
            (vector->list
             (assoc-ref
              (assoc-ref (mumi-graphql-get
                          `(document
                            (query (#(issue #:number ,issue-number)
                                    (messages
                                     subject)))))
                         "issue")
              "messages")))
           ;; Find the latest reroll count if not provided.
           (reroll-count (or reroll-count
                             (match (filter-map (lambda (message)
                                                  (and-let* ((subject (message-subject-ref message))
                                                             (patch (parse-patch-subject subject)))
                                                    (patch-reroll-count patch)))
                                                messages)
                               (()
                                (format (current-error-port) "No patches found~%")
                                (exit #f))
                               (lst (apply max lst))))))
      ;; There may be multiple patchsets with the same reroll count
      ;; (usually created when the contributor forgets to pass the
      ;; --reroll-count option to `git format-patch'). Apply only the
      ;; last patchset with the desired reroll count.
      (match (filter-map (lambda (message-index message)
                           (and-let* ((subject (message-subject-ref message))
                                      (patch (parse-patch-subject subject)))
                             ;; Eliminate cover letters so it's easier
                             ;; to find the latest patchset.
                             (and (not (zero? (patch-patch-index patch)))
                                  (= (patch-reroll-count patch)
                                     reroll-count)
                                  (set-patch-message-index patch message-index))))
                         (iota (length messages))
                         messages)
        (()
         (format (current-error-port) "Patchset v~a not found~%" reroll-count)
         (exit #f))
        (patches
         (for-each (lambda (patch)
                     ;; Download mbox and apply.
                     (if dry-run?
                         (format (current-output-port) "Apply @~a ~a~%"
                                 (patch-message-index patch)
                                 (colorize-string (patch-subject patch)
                                                  'MAGENTA))
                         (let ((_ mbox (http-get (build-mumi-uri (format #f "/issue/~a/raw/~a"
                                                                         issue-number
                                                                         (patch-message-index patch))))))
                           (apply call-with-output-pipe*
                                  (cut put-bytevector <> mbox)
                                  "git" "am" git-am-args))))
                   ;; Guess at the total number of patches using the
                   ;; last patch. Take that many patches and sort them
                   ;; by patch index. This works regardless of the
                   ;; presence or absence of cover letters since we
                   ;; eliminated them earlier.
                   (sort (take-right patches
                                     (patch-total-patches (last patches)))
                         (lambda (patch1 patch2)
                           (< (patch-patch-index patch1)
                              (patch-patch-index patch2))))))))))
