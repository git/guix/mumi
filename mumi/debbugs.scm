;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2023–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi debbugs)
  #:use-module (mumi config)
  #:use-module (mumi cache)
  #:use-module (email email)
  #:use-module (email quoted-printable)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-171)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 regex)
  #:export (read-emails-from-bug-log
            extract-bug-numbers
            bug-id->log-file
            bug-id->summary-file

            bug-status-from-summary
            make-bug-status
            bug-status?

            bug-num
            bug-package
            bug-archived
            bug-blockedby
            bug-blocks
            bug-date
            bug-done
            bug-mergedwith
            bug-originator
            bug-owner
            bug-severity
            bug-subject
            bug-tags))

(define* (read-emails-from-bug-log port
                                   #:key
                                   (keep '(incoming-recv))
                                   raw?)
  "Read the Debbugs bug log from PORT and return a list of parsed
email objects.  Return the raw emails as a list when RAW? is #T.
According to the documentation of the Perl module Debbugs::Log there
are four record types that are separated with single control
characters on a line of their own.

* autocheck

This is for auto-forwarded messages.  The mail is recorded between a
leading ^A and a trailing ^C.

* recips

This is for emails with an additional list of recipients.  The
recipients list may be just the string \"-t\", which represents the
same sendmail option, indicating that the recipients are taken from
the message headers.  This record starts with ^B, uses ^D as an inline
separator for recipients, and uses ^E to mark the beginning of the
email.  The email ends with ^C.

* html

This is for raw HTML snippets.  They are recorded between ^F and ^C.
They are only used for the Debbugs web interface.

* incoming-recv

This is for received emails.  The raw email is introduced by ^G and
ends with ^C.
"
  (let loop ((msgids (make-hash-table))
             (mails '())
             (lines '())
             (type 'init)
             (skip? #f))
    (let ((line (read-line port)))
      (if (eof-object? line) (reverse mails)
          (match (and (not (string-null? line))
                      (string-ref line 0))
            ;; Ctrl-A, autocheck
            (#\soh
             (loop msgids mails lines 'autocheck #f))
            ;; Ctrl-B, recips: skip until beginning of email
            (#\stx
             (loop msgids mails lines 'recips #t))
            ;; Ctrl-C, end of record
            (#\etx
             ;; For some reason Debbugs stores the first two lines out
             ;; of order.  Swap them.  See
             ;; https://issues.guix.gnu.org/41906 for details.
             (let ((mails*
                    (if (member type keep)
                        (let ((lines* (match (reverse lines)
                                        ((first second . rest)
                                         (cons* second first rest))
                                        (other other))))
                          ;; TODO: The first line of the raw messages
                          ;; stored in Debbugs logs seem to confuse
                          ;; the email parser, so we drop it.
                          (let* ((content (string-join (drop lines* 1) "\n"))
                                 (mail (catch #t
                                         (lambda ()
                                           (parse-email content))
                                         (lambda args
                                           (format (current-error-port)
                                                   "failed to process email~%")
                                           #f))))
                            (let ((id (and mail
                                           (email-message-id mail))))
                              (if (and id
                                       (not (hash-ref msgids id)))
                                  (begin
                                    (hash-set! msgids id #t)
                                    (cons (if raw?
                                              (string-join lines* "\n")
                                              mail)
                                          mails))
                                  mails))))
                        mails)))
               (loop msgids mails* '() 'init #f)))
            ;; Ctrl-E, beginning of email in recips
            (#\enq
             (loop msgids mails lines 'recips #f))
            ;; Ctrl-F, raw HTML snippet: skip
            (#\ack
             (loop msgids mails lines 'html #t))
            ;; Ctrl-G, incoming-recv
            (#\bel
             (loop msgids mails lines 'incoming-recv #f))
            (_
             (if skip?
                 (loop msgids mails lines type skip?)
                 (loop msgids mails (cons line lines) type skip?))))))))

(define* (filter-index pred proc #:key archived?)
  "Open up a Debbugs index file and collect the result of applying
PROC to those lines that match the predicate PRED.  If ARCHIVED? is #T
search the index of archived issues."
  (define index-file
    (format #f "~a/spool/index.~a.realtime" (%config 'data-dir)
            (if archived? "archive" "db")))
  (call-with-input-file index-file
    (lambda (port)
      (let loop ((result '()))
        (match (read-line port)
          ((? eof-object? x) result)
          (line
           (cond
            ((pred line)
             (loop (cons (proc line) result)))
            (else (loop result)))))))))

(define* (extract-bug-numbers packages #:key archived?)
  "Open up a Debbugs index file and return the bug numbers for those
lines that match one of PACKAGES.  If ARCHIVED? is #T search the index
of archived issues."
  (define cache-key
    (list 'extract-bug-numbers packages archived?))
  (define cached (cached? cache-key))
  (or cached
      (let ((result
             (filter-index (lambda (line)
                             (any (lambda (package)
                                    (string-prefix? package line))
                                  packages))
                           (lambda (line)
                             (second (string-split line #\space)))
                           #:archived? archived?)))
        (cache! cache-key result)
        result)))

(define* (bug-id->log-file bug-id #:key archived?)
  (format #f "~a/spool/~a/~a/~a.log"
          (%config 'data-dir)
          (if archived? "archive" "db-h")
          (string-take-right (if (string? bug-id)
                                 bug-id
                                 (number->string bug-id)) 2)
          bug-id))

(define (bug-id->summary-file bug-id)
  (let ((candidate (lambda (archived?)
                     (format #f "~a/spool/~a/~2,'0d/~a.summary"
                             (%config 'data-dir)
                             (if archived? "archive" "db-h")
                             (remainder (if (string? bug-id)
                                            (string->number bug-id)
                                            bug-id)
                                        100)
                             bug-id))))
    (find file-exists?
          (list (candidate #f)
                (candidate #t)))))


(define-immutable-record-type <bug-status>
  (make-bug-status num package
                   archived blockedby blocks date done mergedwith
                   originator owner severity subject tags)
  bug-status?
  (num bug-num)
  (package bug-package)
  (archived bug-archived)
  (blockedby bug-blockedby)
  (blocks bug-blocks)
  (date bug-date)
  (done bug-done)
  (mergedwith bug-mergedwith)
  (originator bug-originator)
  (owner bug-owner)
  (severity bug-severity)
  (subject bug-subject)
  (tags bug-tags))

(define (bug-status-from-summary bug-id)
  (and-let* ((file (bug-id->summary-file bug-id))
             (properties
              (call-with-input-file file
                (lambda (port)
                  (port-transduce (tmap (lambda (line)
                                          (let* ((split-point (string-index line #\:))
                                                 (key (string-take line split-point))
                                                 (value (string-drop line (1+ split-point))))
                                            (cons key
                                                  (string-trim-both value)))))
                                  rcons
                                  read-line
                                  port)))))
    (make-bug-status bug-id
                     (assoc-ref properties "Package")
                     (string-contains file "/archive/")
                     (let ((blocked-by (assoc-ref properties "Blocked-By")))
                       (if blocked-by
                           ;; Sometimes (possibly due to Debbugs
                           ;; errors), the Blocked-By field refers to
                           ;; non-existent bugs (for example, see bug
                           ;; 70456). Filter them out.
                           (filter bug-id->summary-file
                                   (map string->number
                                        (string-split blocked-by #\space)))
                           (list)))
                     (assoc-ref properties "Blocks")
                     (time-monotonic->date
                      (make-time time-monotonic
                                 0
                                 (string->number (assoc-ref properties "Date"))))
                     (assoc-ref properties "Done")
                     (let ((merged-with (assoc-ref properties "Merged-With")))
                       (if merged-with
                           (map string->number
                                (string-split merged-with #\space))
                           (list)))
                     (assoc-ref properties "Submitter")
                     (assoc-ref properties "Owner")
                     (or (assoc-ref properties "Severity") "normal")
                     (assq-ref
                      (parse-email-headers
                       (string-append "Subject: "
                                      (or (assoc-ref properties "Subject") "")
                                      "\n"))
                      'subject)
                     (let ((tags (assoc-ref properties "Tags")))
                       (if tags
                           (string-split tags #\space)
                           (list))))))
