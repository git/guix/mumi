;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This file is part of mumi.
;;;
;;; mumi is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; mumi is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with mumi.  If not, see <http://www.gnu.org/licenses/>.

(define-module (mumi email)
  #:use-module (rnrs bytevectors)
  #:use-module (email email)
  #:export (email->text))

(define (email->text email)
  "Render EMAIL to plain text."
  (define (mime-entity->text entity)
    (cond
     ;; Ignore HTML and binary entities.
     ((or (let ((content-type (assq-ref (mime-entity-headers entity)
                                        'content-type)))
            (and (eq? (assq-ref content-type 'type)
                      'text)
                 (eq? (assq-ref content-type 'subtype)
                      'html)))
          (bytevector? (mime-entity-body entity)))
      "")
     ;; Simple entity with string body
     ((string? (mime-entity-body entity))
      (mime-entity-body entity))
     ;; Recurse over multipart entity.
     (else
      (string-join (map mime-entity->text
                        (mime-entity-body entity))
                   "\n"))))

  (cond
   ;; Iterate over parts of multipart email.
   ((eq? (assoc-ref (email-content-type email)
                    'type)
         'multipart)
    (string-join (map mime-entity->text
                      (email-body email))
                 "\n"))
   ;; Simple email with string body
   (else
    (mime-entity->text
     (make-mime-entity (email-headers email)
                       (email-body email))))))
