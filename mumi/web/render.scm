;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2016, 2017, 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2014 David Thompson <davet@gnu.org>
;;; Copyright © 2023, 2025 Arun Isaac <arunisaac@systemreboot.net>
;;; Copyright © 2024 Felix Lechner <felix.lechner@lease-up.com>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;; This code was snarfed from David Thompson's guix-web.

(define-module (mumi web render)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 binary-ports)
  #:use-module ((ice-9 textual-ports)
                #:select (get-string-all put-string))
  #:use-module (ice-9 match)
  #:use-module (web http)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (web uri)
  #:use-module (webutils multipart)
  #:use-module (mumi config)
  #:use-module (mumi web sxml)
  #:use-module (mumi web util)
  #:export (render-static-asset
            render-html
            not-found
            unprocessable-entity
            created
            redirect
            parse-form-submission))

(define file-mime-types
  '(("css" . (text/css))
    ("js"  . (text/javascript))
    ("svg" . (image/svg+xml))
    ("png" . (image/png))
    ("gif" . (image/gif))
    ("woff" . (application/font-woff))
    ("ttf"  . (application/octet-stream))
    ("html" . (text/html))))

(define (render-static-asset request)
  (render-static-file (%config 'assets-dir) request))

(define %not-slash
  (char-set-complement (char-set #\/)))

(define (render-static-file root request)
  (define path
    (uri-path (request-uri request)))

  (define failure
    (call-with-values
        (cut not-found path)
      list))

  (let ((file-name (string-append root "/" path)))
    (apply values
           (if (not (any (cut string-contains <> "..")
                         (string-tokenize path %not-slash)))
               (let* ((stat (stat file-name #f))
                      (modified (and stat
                                     (make-time time-utc 0 (stat:mtime stat)))))
                 (define (send-file)
                   (list `((content-type
                            . ,(assoc-ref file-mime-types
                                          (file-extension file-name)))
                           (last-modified . ,(time-utc->date modified)))
                         (call-with-input-file file-name get-bytevector-all)))

                 (if (and stat (not (eq? 'directory (stat:type stat))))
                     (cond ((assoc-ref (request-headers request) 'if-modified-since)
                            =>
                            (lambda (client-date)
                              (if (time>? modified (date->time-utc client-date))
                                  (send-file)
                                  (list (build-response #:code 304) ;"Not Modified"
                                        #f))))
                           (else
                            (send-file)))
                     failure))
               failure))))

(define* (render-html sxml #:key (response-code 200) (extra-headers '()))
  (values (build-response
           #:code response-code
           #:headers (append extra-headers
                             '((content-type
                                . (text/html (charset . "utf-8"))))))
          (call-with-output-string
	    (lambda (port)
              (sxml->html sxml port)))))

(define (not-found path)
  (values (build-response #:code 404)
          (string-append "Resource not found: " path)))

(define (unprocessable-entity)
  (values (build-response #:code 422)
          ""))

(define (created)
  (values (build-response #:code 201)
          ""))

;; We need to override the writer for Location header values, because
;; write-uri drops the fragment.  Since we want to be able to redirect
;; to URLs with a fragment to jump to an anchor we need to replace the
;; header definition.
(define (write-uri* uri port)
  (put-string port (uri->string uri #:include-fragment? #true)))

(declare-header! "Location"
                 (lambda (str)
                   (or (string->uri-reference str)
                       (throw 'bad-header-component 'uri-reference str)))
                 uri-reference?
                 write-uri*)

(define* (redirect path #:key query fragment (headers '()))
  (let ((uri (build-relative-ref
              #:path (string-append
                      "/" (encode-and-join-uri-path path))
              #:query query
              #:fragment fragment)))
    (values (build-response
             #:code 302
             #:headers (append `((content-type . (text/html))
                                 (location . ,uri))
                               headers))
            (format #f "Redirect to ~a" (uri->string uri)))))

(define (parse-form-submission request body)
  "Return an alist with keys to values for the submitted form."
  (let ((parts (parse-request-body request body)))
    (map (lambda (part)
           (let ((binary? (match (assoc-ref (part-headers part) 'content-type)
                            (('application/octet-stream . _) #t)
                            (_ #f))))
             (and-let*
                 ((content (assoc-ref (part-headers part) 'content-disposition))
                  (form-data (and=> (memq 'form-data content) cadr))
                  (name (and=> (memq 'name form-data) cdr)))
               (cons (string->symbol name)
                     ((if binary? get-bytevector-all get-string-all)
                      (part-body part))))))
         parts)))
