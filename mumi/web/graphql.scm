;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2022–2025 Arun Isaac <arunisaac@systemreboot.net>
;;; Copyright © 2023 Christopher Baines <mail@cbaines.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi web graphql)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (kolam graphql)
  #:use-module (kolam http)
  #:use-module (email email)
  #:use-module (mumi issue)
  #:use-module (mumi messages)
  #:use-module (mumi xapian)
  #:export (handle-graphql))

(define-enum-type <severity>
  "critical" "grave" "serious" "important"
  "normal" "minor" "wishlist")

(define (accessor->resolver accessor)
  (lambda (parent . _)
    (accessor parent)))

(define-object-type <issue>
  (number (non-nullable-type <integer>)
          (accessor->resolver issue-number))
  (package (non-nullable-type <string>)
           (accessor->resolver issue-package))
  (title (non-nullable-type <string>)
         (accessor->resolver issue-title))
  (date (non-nullable-type <datetime>)
        (accessor->resolver issue-date))
  (last_updated (non-nullable-type <datetime>)
                (accessor->resolver issue-last-updated))
  (open (non-nullable-type <boolean>)
        (lambda (parent . _)
          (issue-open? parent)))
  (submitter (non-nullable-type <person>)
             (accessor->resolver issue-submitter))
  (closer <person>
          (lambda (parent . _)
            (or (issue-closer parent)
                'null)))
  (severity (non-nullable-type <severity>)
            (accessor->resolver issue-severity))
  (tags (non-nullable-type (list-type <string>))
        (lambda (parent . _)
          (issue-tags parent)))
  (messages (non-nullable-type (list-type <email>))
            (lambda (parent . _)
              (issue-messages (issue-number parent))))
  (blocked_by (non-nullable-type (list-type <issue>))
              (lambda (parent . _)
                (map find-issue (issue-blocked-by parent))))
  (merged_with (non-nullable-type (list-type <issue>))
               (lambda (parent . _)
                 (map find-issue (issue-merged-with parent)))))

(define-object-type <person>
  (name <string> (lambda (parent . _)
                   (or (assq-ref (parse-email-address parent)
                                 'name)
                       'null)))
  (address (non-nullable-type <string>)
           (lambda (parent . _)
             (assq-ref (parse-email-address parent)
                       'address)))
  (submitted_issues (non-nullable-type (list-type <issue>))
                    (lambda (parent . _)
                      (search-bugs (string-append "submitter:" parent))))
  (participated_in_issues (non-nullable-type (list-type <issue>))
                          (lambda (parent . _)
                            (search-bugs (string-append "author:" parent)))))

(define-object-type <email>
  (message_id (non-nullable-type <id>)
              (accessor->resolver email-message-id))
  (from (non-nullable-type <person>)
        (lambda (parent . _)
          (match (email-from parent)
            ((from _ ...) (interpret-address from)))))
  (date (non-nullable-type <datetime>)
        (accessor->resolver email-date))
  (subject <string> (lambda (parent . _)
                      (or (email-subject parent)
                          'null))))

(define-object-type <query>
  (issue (non-nullable-type <issue>)
         (lambda* (parent context info #:key number)
           (find-issue number)))
  (issues (non-nullable-type (list-type <issue>))
          (lambda* (parent context info #:key search)
            (search-bugs search)))
  (person (non-nullable-type <person>)
          (lambda* (parent context info #:key email)
            email)))

(define schema
  (graphql-schema #:query <query>))

(define handle-graphql
  (graphql-handler schema))
