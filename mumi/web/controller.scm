;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2016-2022 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2022–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi web controller)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (rnrs exceptions)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (web uri)
  #:use-module (webutils sessions)
  #:use-module (gcrypt base64)
  #:use-module (gcrypt mac)
  #:use-module (mumi config)
  #:use-module ((mumi xapian) #:select (find-issue))
  #:use-module (mumi issue)
  #:use-module (mumi jobs)
  #:use-module (mumi messages)
  #:use-module (mumi web render)
  #:use-module (mumi web download)
  #:use-module (mumi web graphql)
  #:use-module (mumi web util)
  #:use-module (mumi web view html)
  #:use-module ((mumi xapian) #:select (parse-search-query search-query search match-count))
  #:use-module (xapian xapian)
  #:export (controller))

(define-syntax-rule (-> target functions ...)
  (fold (lambda (f val) (and=> val f))
        target
        (list functions ...)))

(define (%session-manager)
  (let ((key-file (string-append (%config 'key-dir) "/signing-key")))
    (unless (file-exists? key-file)
      (with-output-to-file key-file
        (lambda () (write (base64-encode (generate-signing-key))))))
    (make-session-manager
     (with-input-from-file key-file read)
     ;; expire session after 30 mins
     #:expire-delta '(0 0 30))))

(define* (search-results-page query-string
                              #:optional
                              (package (assq-ref (%server-config)
                                                 'default-package))
                              (status 'open))
  "Return search results page for QUERY-STRING showing only issues with STATUS.
STATUS is either 'open or 'closed."
  (let* ((parsed-query (parse-search-query query-string))
         (package-query (query (string-append "XP" package)))
         ;; FIXME: We should be using query-filter, not query-and. But
         ;; query-filter messes up the order of the subqueries, and thus the
         ;; sort order of the results. Switch to query-filter once that is done.
         (open-query (query-and parsed-query
                                (query "XSTATUSopen")
                                package-query))
         (closed-query (query-and parsed-query
                                  (query "XSTATUSdone")
                                  package-query)))
    (render-html
     (list-of-matching-bugs
      query-string
      (search-query (case status
                      ((open) open-query)
                      ((closed) closed-query)))
      (match-count open-query)
      (match-count closed-query)
      status
      package))))

(define (controller request body)
  (define handle-request
    (match-lambda*
      ;; Handle HEAD requests like GET requests, but don't return the
      ;; response body.
      (('HEAD path-components ...)
       (let ((response body (apply handle-request 'GET path-components)))
         (values response #f)))
      (('GET)
       (render-html (index (assq-ref (%server-config) 'default-package))
                    #:extra-headers
                    '((cache-control . ((max-age . 60))))))
      (('GET (? (cute member <> (map (match-lambda
                                       ((package _ ...) package))
                                     (assq-ref (%server-config) 'packages)))
                package))
       (render-html (index package)
                    #:extra-headers '((cache-control . ((max-age . 60))))))
      ;; These easy, recent, forgotten, priority and wishlist pages should
      ;; really be deprecated, but we keep them alive in the interest of not
      ;; breaking any URIs.
      (('GET "easy")
       (search-results-page "tag:easy"))
      (('GET "recent")
       (search-results-page "mdate:1m.."))
      (('GET "forgotten")
       (search-results-page "mdate:..30d is:open"))
      (('GET "priority")
       (search-results-page "(severity:serious OR severity:important) is:open"))
      (('GET "wishlist")
       (search-results-page "severity:wishlist is:open"))
      (((or 'GET 'POST) "graphql")
       (handle-graphql request body))
      (('GET "search")
       (let* ((query-parameters (-> request
                                    request-uri
                                    uri-query
                                    parse-query-string))
              (query (assoc-ref query-parameters "query")))
         (cond
          ;; TODO: query should not be empty!
          ((or (not query)
               (string-null? (string-trim query)))
           (redirect '()))

          ;; For convenience
          ((string-prefix? "id:" query) =>
           (lambda _ (redirect (list "issue" (string-drop query (string-length "id:"))))))
          ((string-prefix? "#" query) =>
           (lambda _ (redirect (list "issue" (string-drop query (string-length "#"))))))
          ((string->number query) =>
           (lambda _ (redirect (list "issue" query))))

          ;; Search for matching messages and return list of bug reports
          ;; that belong to them.
          (else
           (search-results-page query
                                (or (assoc-ref query-parameters "package")
                                    (assq-ref (%server-config) 'default-package))
                                ;; We fall back to open issues when the "is"
                                ;; parameter is absent or is invalid.
                                (if (equal? (assoc-ref query-parameters "is")
                                            "closed")
                                    'closed
                                    'open))))))
      ((or ('GET "issue" (? string->number id))
           ('GET (? string->number id)))
       (let ((issue (find-issue (string->number id)))
             (plain? (-> request
                         request-uri
                         uri-query
                         parse-query-string
                         (cut assoc-ref <> "plain")))
             (message (match (uri-query (request-uri request))
                        ("comment-ok"
                         '(info . "Your comment has been submitted!"))
                        ("comment-error"
                         '(error . "There was an error submitting your comment!"))
                        (_ #f))))
         (if issue
             ;; Record the current issue id in an encrypted cookie.
             ;; This will be verified when posting a comment.
             (let* ((cookie-header
                     (set-session (%session-manager) `((issue-id . ,id))))
                    (other-headers
                     (cond
                      ((issue-archived issue)
                       ;; Tell browser to cache this for 12 hours.
                       '((cache-control . ((max-age . 43200)))))
                      ((issue-open? issue)
                       ;; Tell browser to cache this for 1 hour.
                       '((cache-control . ((max-age . 3600)))))
                      (else
                       ;; Tell browser to cache this for 5 minutes.
                       '((cache-control . ((max-age . 300)))))))
                    (headers (if (assq-ref (%server-config) 'mailer-enabled?)
                                 (cons cookie-header
                                       other-headers)
                                 other-headers))
                    (page (issue-page issue
                                      #:flash-message message
                                      #:plain? plain?)))
               (if page
                   (render-html page #:extra-headers headers)
                   (render-html (unknown id))))
             (render-html (unknown id)))))
      (('GET "msgid" msgid)
       (match (search (format #false "msgid:~a" msgid))
         ((first-issue . rest)
          (redirect (list "issue" (number->string (issue-number first-issue)))
                    #:fragment (format #false "msgid-~a" (msgid-hash msgid))))
         (_ (render-html (unknown msgid)))))
      (('POST "issue" (? string->number id) "comment")
       (if (assq-ref (%server-config) 'mailer-enabled?)
           (let ((headers   (request-headers request))
                 (form-data (parse-form-submission request body))
                 (cookie    (or (session-data (%session-manager) request)
                                '()))
                 (issue     (find-issue (string->number id))))
             (if (and
                  issue
                  ;; The encrypted cookie must be fresh and contain the
                  ;; current issue id.
                  (and=> (assoc-ref cookie 'issue-id)
                         (cut string=? id <>))
                  ;; The honeypot field "validation" must remain empty
                  (let ((val (assoc-ref form-data 'validation)))
                    (and val (string-null? (string-trim-both val))))
                  ;; Submission may not have happened too quickly
                  (let ((time (assoc-ref form-data 'timestamp)))
                    (and time (reasonable-timestamp? time)))
                  ;; Message must not be too short
                  (and=> (assoc-ref form-data 'text)
                         (lambda (text)
                           (> (string-length (string-trim-both text)) 10)))
                  ;; Message must have sender
                  (and=> (assoc-ref form-data 'from)
                         (compose (negate string-null?) string-trim-both)))
                 (begin
                   ;; Send comment to list
                   (enqueue 'mail
                            `((from . ,(string-trim-both (assoc-ref form-data 'from)))
                              (subject . ,(issue-title issue))
                              (to . ,(format #f "~a@~a"
                                             id (%config 'debbugs-domain)))
                              (text . ,(assoc-ref form-data 'text))))
                   (redirect (list "issue" id) #:query "comment-ok"))
                 (redirect (list "issue" id) #:query "comment-error")))
           (redirect (list "issue" id) #:query "comment-error")))
      (('GET "issue" (? string->number id)
             "attachment" (? string->number msg-num)
             (? string->number path) ...)
       (handle-download (string->number id)
                        (string->number msg-num)
                        (map string->number path)))
      (('GET "issue" (? string->number id)
             "patch-set" . patch-set-num)
       (let* ((patch-set-num* (match patch-set-num
                                (() #false)
                                ((f . rest) f)))
              (content (patch-messages id patch-set-num*)))
         (if content
             (values `((content-type application/mbox)
                       (content-disposition
                        text/plain
                        (filename
                         . ,(format #f "~a-patch-set~:[-~a~;~].mbox"
                                    id patch-set-num* patch-set-num*))))
                     content)
             (not-found (uri-path (request-uri request))))))
      (('GET "issue" (? string->number id)
             "raw" (? string->number msg-num))
       (download-raw (string->number id)
                     (string->number msg-num)))
      (('GET "issue" not-an-id)
       (render-html (unknown not-an-id)))
      (('GET "help")
       (render-html (help)
                    ;; Cache for 24 hours.
                    #:extra-headers
                    '((cache-control . ((max-age . 86400))))))
      (('GET path ...)
       (render-static-asset request))))

  (with-exception-handler
      (lambda (exn)
        (values (build-response #:code 500)
                #f))
    (lambda ()
      (with-exception-handler
          (lambda (exn)
            (let ((stack (make-stack #t)))
              (display
               (call-with-output-string
                 (lambda (port)
                   (display-backtrace stack port)
                   (print-exception
                    port
                    (stack-ref stack 1)
                    '%exception
                    (list exn))))
               (current-error-port)))
            (raise-exception exn))
        (lambda ()
          (apply handle-request
                 (request-method request)
                 (request-path-components request)))))
    #:unwind? #t))
