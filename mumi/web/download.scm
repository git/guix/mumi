;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2018, 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2019, 2023–2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi web download)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (email email)
  #:use-module (ice-9 iconv)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (mumi debbugs)
  #:use-module (mumi messages)
  #:use-module (mumi web render)
  #:use-module (mumi web view html)
  #:export (handle-download
            download-raw))

(define (handle-download id msg-num path)
  "Handle download of an attachment for bug ID, message number
MSG-NUM, in the possibly nested message part identified by the list
PATH."
  (define (html-response headers body)
    (values (filter-map (match-lambda
                          (('content-type . vals)
                           (list 'content-type
                                 (or (assoc-ref vals 'type) 'text)))
                          (('content-disposition . vals)
                           (list 'content-disposition
                                 (assoc-ref vals 'type)
                                 `(filename . ,(or (assoc-ref vals 'filename)
                                                   "attachment"))))
                          (_ #f))
                        headers)
            body))

  (match (extract-attachment id msg-num path)
    ((? mime-entity? entry)
     (html-response (mime-entity-headers entry)
                    (mime-entity-body entry)))
    ((? email? entry)
     (html-response (email-headers entry)
                    (email-body entry)))
    (_
     (render-html (unknown id)
                  #:response-code 404))))

(define (download-raw id msg-num)
  "Handle download of an attachment for bug ID, message number
MSG-NUM, in the possibly nested message part identified by the list
PATH."
  (define (get-emails file)
    (call-with-input-file file
      (cut read-emails-from-bug-log <> #:raw? #t)))
  (define emails
    (let* ((candidate (lambda (archived?)
                        (bug-id->log-file id #:archived? archived?)))
           (file (find file-exists?
                       (list (candidate #f)
                             (candidate #t)))))
      (and file (get-emails file))))
  (if (and emails (> (length emails) msg-num))
      (values `((content-type application/mbox)
                (content-disposition text/plain
                                     (filename . ,(format #f "~a-~a.mbox" id msg-num))))
              (list-ref emails msg-num))
      (render-html (unknown id)
                   #:response-code 404)))
