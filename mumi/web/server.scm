;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2017, 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2021, 2023, 2025 Arun Isaac <arunisaac@systemreboot.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi web server)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (web http)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (web uri)
  #:use-module (web server)
  #:use-module (mumi web controller)
  #:use-module (mumi web util)
  #:export (start-mumi-web-server))

(define (handler request body)
  (let ((response body (controller request body)))
    (format #t "~a ~a ~a\n"
            (request-method request)
            (uri->string (request-uri request))
            (if (response? response)
                (response-code response)
                200))
    (values response body)))

(define (start-mumi-web-server address port)
  (format (current-error-port)
          "Mumi web server listening on http://~a:~a/~%"
          address port)
  ;; Wrap handler in another function to support live hacking via the
  ;; REPL. If handler is passed as is and is then redefined via the
  ;; REPL, the web server will still be using the old handler. The
  ;; only way to update the handler reference held by the web server
  ;; would be to restart the web server.
  (run-server (cut handler <> <>)
              'http
              (list #:addr (inet-pton AF_INET address)
                    #:port port)))
