;;; mumi -- Mediocre, uh, mail interface
;;; Copyright © 2017, 2018, 2019, 2020 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2018, 2019, 2025 Arun Isaac <arunisaac@systemreboot.net>
;;; Copyright © 2018 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2024 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (mumi web view utils)
  #:use-module (ice-9 format)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 iconv)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (syntax-highlight)
  #:use-module (syntax-highlight scheme)
  #:use-module (email email)
  #:use-module (mumi messages)
  #:use-module (rnrs bytevectors)
  #:use-module (web uri)
  #:export (prettify
            copy-icon
            download-icon
            display-message-body
            time->string))

(define-record-type <block>
  (make-block type lines)
  block?
  (type   block-type)
  (lines  block-raw-lines set-block-lines!))

(define (block-lines block)
  (reverse (block-raw-lines block)))

(define (add-block-line! block line)
  (set-block-lines! block
                    (cons line (block-raw-lines block)))
  block)

(define (process line blocks context message-number line-number)
  "Process the current LINE.  Add it to the latest block in BLOCKS or
add it to a new block, dependent on CONTEXT.  Return the blocks along
with the next context."
  (define line-info
    `((id ,(format #false "~a-lineno~a"
                   message-number line-number))))
  (match blocks
    ((block . other-blocks)
     (cond
      ((string-null? line)
       (values (cons (add-block-line! block `(br))
                     other-blocks)
               context))
      ((eq? context 'snippet)
       (if (string-prefix? "--8<---------------cut here" line)
           (values blocks #f)
           (values (cons (add-block-line! block
                                          `(div (@ (class "line")) ,line))
                         other-blocks)
                   context)))
      ((eq? context 'diff)
       (if (string= "--" line)
           ;; Retry
           (process line blocks #f message-number line-number)
           ;; Format line and add to current block
           (let ((formatted-line
                  (cond
                   ((string= "---" line)
                    `(div (@ (class "line diff separator") ,@line-info)
                          ,line))
                   ((string-prefix? "+" line)
                    `(div (@ (class "line diff addition") ,@line-info)
                          ,line))
                   ((and (string-prefix? "-" line)
                         (not (string= "--" line))
                         (not (string= "-- " line)))
                    `(div (@ (class "line diff deletion") ,@line-info)
                          ,line))
                   ((string-prefix? "@@" line)
                    `(div (@ (class "line diff range") ,@line-info)
                          ,line))
                   (else
                    `(div (@ (class "line") ,@line-info) ,line)))))
             (values (cons (add-block-line! block formatted-line)
                           other-blocks)
                     context))))
      ((eq? context 'quote)
       (if (string-prefix? ">" line)
           ;; Add line to current block
           (values (cons (add-block-line! block
                                          `(div (@ (class "line") ,@line-info)
                                                ,line))
                         other-blocks)
                   context)
           ;; Retry
           (process line blocks #f message-number line-number)))
      (else
       (let ((new-context
              (cond
               ((string-prefix? "diff --git" line)
                'diff)
               ((string-prefix? ">" line)
                'quote)
               ((string-prefix? "--8<---------------cut here" line)
                'snippet)
               (else 'text)))
             (formatted-line
              (cond
               ((or (string-contains line "https://")
                    (string-contains line "http://")) =>
                    (lambda (index)
                      (let* ((pre (string-take line index))
                             (post (string-drop line index))
                             (uri+ (string-split post (char-set #\< #\> #\space))))
                        (match uri+
                          ((uri-string . rest)
                           (or (and=> (string->uri uri-string)
                                      (lambda (uri)
                                        `(div (@ (class "line") ,@line-info)
                                              ,(string-trim-right pre #\<)
                                              (a (@ (href ,uri-string))
                                                 ,uri-string)
                                              ,(string-join rest " "))))
                               `(div (@ (class "line") ,@line-info)
                                     ,line)))))))
               ((or (string-prefix? "Signed-off-by" line)
                    (string-prefix? "Co-authored-by" line))
                `(div (@ (class "line commit attribution") ,@line-info)
                      ,line))
               ((or (string-prefix? "From: " line)
                    (string-prefix? "Date: " line)
                    (string-prefix? "Subject: " line))
                `(div (@ (class "line commit header") ,@line-info)
                      ,line))
               ((or (string-prefix? "* " line)
                    (string-prefix? " * " line))
                `(div (@ (class "line commit changelog") ,@line-info)
                      ,line))
               ((string-prefix? "diff --git" line)
                `(div (@ (class "line diff file") ,@line-info)
                      ,line))
               ((string-prefix? "--8<---------------cut here" line)
                "")
               (else
                `(div (@ (class "line") ,@line-info) ,line)))))
         (if (eq? new-context context)
             (values (cons (add-block-line! block formatted-line)
                           other-blocks)
                     context)
             (values (cons (make-block new-context (list formatted-line))
                           blocks)
                     new-context))))))))

(define (prettify text message-number)
  "Read each line of TEXT and apply PROCESS to it.  Prefix line
numbers with the given MESSAGE-NUMBER."
  (let* ((lines (string-split text #\newline))
         (res (fold (lambda (line line-number acc)
                      (call-with-values
                          (lambda ()
                            (process line
                                     (cadr (memq #:blocks acc))
                                     (cadr (memq #:context acc))
                                     message-number line-number))
                        (lambda (new-blocks new-context)
                          `(#:blocks ,new-blocks #:context ,new-context))))
                    (list #:blocks (list (make-block 'text '()))
                          #:context 'text)
                    lines
                    (iota (length lines)))))
    (map (lambda (block)
           (if (eq? 'text (block-type block))
               `(div (@ (class ,(format #f "block ~a" (block-type block))))
                     ,(block-lines block))
               `(details (@ (class ,(format #f "block ~a" (block-type block)))
                            (open "open"))
                         (summary ,(format #f "Toggle ~a (~a lines)" (block-type block)
                                           (length (block-raw-lines block))))
                         ,(block-lines block))))
         (reverse (cadr (memq #:blocks res))))))

(define (content-type->css-class value)
  "Convert a content-type header value to a CSS class name."
  (string-append (symbol->string (assoc-ref value 'type))
                 "-"
                 (symbol->string (assoc-ref value 'subtype))))

;; https://icons.getbootstrap.com/icons/download/
(define download-icon
  '(svg (@ (class "bi bi-download")
           (viewBox "0 0 16 16")
           (fill "currentColor")
           (xmlns "http://www.w3.org/2000/svg"))
        (path (@ (d "M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 \
1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 \
1-2-2v-2.5a.5.5 0 0 1 .5-.5")))
        (path (@ (d "M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 \
0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 \
1 0-.708.708z")))))

;; https://icons.getbootstrap.com/icons/copy/
(define copy-icon
  '(svg (@ (class "bi bi-copy")
           (viewBox "0 0 16 16")
           (fill "currentColor")
           (xmlns "http://www.w3.org/2000/svg"))
        (path (@ (fill-rule "evenodd")
                 (d "M4 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H6a2 \
2 0 0 1-2-2zm2-1a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 \
0-1-1zM2 5a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1v-1h1v1a2 2 0 0 1-2 \
2H2a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h1v1z")))))

(define* (display-message-body bug-num message-number message #:optional plain?)
  "Convenience procedure to render MESSAGE (part of bug with number
BUG-NUM), even when it is a multipart message.  Do not prettify
lines when PLAIN? is #T."
  (define truncation-limit 20000)
  (define (display-multipart-chunk headers body path)
    (define (attachment-url)
      (string-append "/issue/"
                     (number->string bug-num)
                     "/attachment/"
                     (number->string message-number)
                     "/" (string-join (map number->string path) "/")))
    (let* ((content-type (assoc-ref headers 'content-type))
           (html?
            (and content-type
                 (eq? 'html (assoc-ref content-type 'subtype))))
           (attachment?
            (and (and=> (assoc-ref headers 'content-disposition)
                        (cut assoc-ref <> 'type))
                 content-type
                 (assoc-ref content-type 'type)))
           (hide-attachment? (and attachment?
                                  (or (member (assoc-ref content-type 'type)
                                              '(application image video))
                                      (and=> (assoc-ref headers 'content-disposition)
                                             (lambda (disposition)
                                               (and=> (assoc-ref disposition 'size)
                                                      (lambda (size)
                                                        (> size 5000)))))
                                      (> (string-length body) 5000))))
           (attachment-name
            (or (and=> (assoc-ref headers 'content-disposition)
                       (cut assoc-ref <> 'filename))
                "file"))
           (truncate-text?
            (and (not attachment?)
                 (> (string-length body) truncation-limit))))
      (cond
       ((or html? hide-attachment?)
        `(div (@ (class "attachment"))
              "Attachment: "
              (a (@ (href ,(attachment-url))) ,attachment-name)
              ,(or (and=> (assoc-ref headers 'content-disposition)
                          (lambda (disposition)
                            (and=> (assoc-ref disposition 'size)
                                   (lambda (bytes)
                                     (format #f " (~1,2f MiB)"
                                             (exact->inexact
                                              (/ bytes 1024 1024)))))))
                   "")))
       ((string-suffix? ".scm" attachment-name)
        `(div (@ (class "multipart scheme"))
              (div (@ (id "download-scheme-part-button")
                      (class "download-part message-button")
                      (data-tooltip "Download Scheme file"))
                   (a (@ (href ,(attachment-url)))
                      ,download-icon))
              ,(highlights->sxml (highlight lex-scheme body))))
       (else
        `(div (@ (class ,(string-join
                          (list "multipart" (or (and content-type
                                                     (content-type->css-class content-type))
                                                "")))))
              (div (@ (id "download-part-button")
                      (class "download-part message-button")
                      (data-tooltip "Download MIME part"))
                   (a (@ (href ,(attachment-url)))
                      ,download-icon))
              ,(cond
                (plain?
                 `(pre (@ (class "ugly-body")) ,body))
                (truncate-text?
                 `(,(prettify (string-take body truncation-limit) message-number)
                   (div
                    (@ (class "truncation-notice"))
                    "This message was truncated. "
                    (a (@ (href ,(attachment-url)))
                       "Download the full message here."))))
                (else
                 (prettify body message-number))))))))
  (define (display-mime-entity entity . path)
    (match entity
      (($ <mime-entity> headers (? string? body))
       (apply display-multipart-chunk `(,headers ,body ,path)))
      ;; Message parts can be nested.
      (($ <mime-entity> headers (? list? sub-parts))
       (map (lambda (part sub-part-num)
              (apply display-mime-entity part
                     (append path (list sub-part-num))))
            sub-parts
            (iota (length sub-parts))))
      ;; XXX: Sometimes we get an unparseable bytevector.  Convert it
      ;; when that happens.
      (($ <mime-entity> headers (? bytevector? raw))
       (apply display-multipart-chunk
              `(,headers
                ,(bytevector->string raw "ISO-8859-1")
                ,path)))))
  (cond
   ((multipart-message? message)
    (let ((parts (email-body message)))
      (map (lambda (part part-num)
             (display-mime-entity part part-num))
           parts
           (iota (length parts)))))
   ;; Regular message with a simple body.
   (else
    (display-mime-entity
     (make-mime-entity (email-headers message)
                       (email-body message))))))

(define (time->string time)
  "Return a string representing TIME in a concise, human-readable way."
  (define seconds
    (time-second
     (if (date? time)
         (date->time-utc time)
         time)))

  (define now*
    (current-time time-utc))

  (define now
    (time-second now*))

  (define elapsed
    (- now seconds))

  (cond ((< elapsed 120)
         "seconds ago")
        ((< elapsed 7200)
         (let ((minutes (inexact->exact
                         (round (/ elapsed 60)))))
           (format #f "~a minutes ago" minutes)))
        ((< elapsed (* 48 3600))
         (let ((hours (inexact->exact
                       (round (/ elapsed 3600)))))
           (format #f "~a hours ago" hours)))
        ((< elapsed (* 3600 24 7))
         (let ((days (inexact->exact
                      (round (/ elapsed 3600 24)))))
           (format #f "~a days ago" days)))
        ((< elapsed (* 3600 24 30))
         (let ((weeks (inexact->exact
                       (round (/ elapsed 3600 24 7)))))
           (format #f "~a weeks ago" weeks)))
        ;; 365.2425 is the average number of days per year in the
        ;; Gregorian calendar.
        ((< elapsed (* 3600 24 365.2425))
         (let ((months (inexact->exact
                        (round (/ elapsed 3600 24 (/ 365.2425 12))))))
           (format #f "~a months ago" months)))
        (else
         (let ((years (inexact->exact
                       (round (/ elapsed 3600 24 365.2425)))))
           (format #f "~a years ago" years)))))
