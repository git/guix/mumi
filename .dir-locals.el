;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((scheme-mode
  (fill-column . 80)
  (eval . (put 'call-with-database 'scheme-indent-function 1))
  (eval . (put 'call-with-input-pipe 'scheme-indent-function 1))
  (eval . (put 'call-with-input-pipe* 'scheme-indent-function 1))
  (eval . (put 'trace-calls 'scheme-indent-function 1))
  (eval . (put 'with-variables 'scheme-indent-function 1))))
