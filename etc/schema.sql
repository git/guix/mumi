BEGIN TRANSACTION;

CREATE TABLE bugs (
  id          INTEGER NOT NULL PRIMARY KEY UNIQUE,
  submitter   TEXT NOT NULL,
  owner       TEXT,
  severity    TEXT NOT NULL,
  status      TEXT NOT NULL,
  tags        TEXT NOT NULL
);

CREATE INDEX bugs_index ON bugs(id, submitter, owner, severity, status, tags);

COMMIT;
